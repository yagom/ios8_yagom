# README #

한빛미디어 리얼타임 [iOS 8 핵심 노트] 예제 소스입니다.

### 문의 및 요청사항 ###

한빛미디어 책 소개 페이지([http://www.hanbit.co.kr/ebook/look.html?isbn=9788968487491](http://www.hanbit.co.kr/ebook/look.html?isbn=9788968487491)) 또는 제 이메일([yagomsoft@gmail.com](yagomsoft@gmail.com))로 보내주시면 감사하겠습니다 :)