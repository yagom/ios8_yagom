//
//  ViewController.swift
//  Today
//
//  Created by yagom on 2015. 4. 3..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // 화면에 IBOutlet으로 연결되는 텍스트 필드
    @IBOutlet weak var textField: UITextField!

    // 화면에 Save 버튼을 눌렀을 때 호출되는 함수
    @IBAction func clickSaveButton(sender: AnyObject) {

        // 만약에 필드에 텍스트가 들어있다면
        if self.textField.text != nil {
            
            // App group의 User Default를 로드합니다.
            let sharedDefault = NSUserDefaults(suiteName: "group.yagomWidget")
            
            // User Default에 필드의 텍스트를 myText라는 키로 지정해줍니다.
            sharedDefault?.setObject(self.textField.text, forKey: "myText")
            
            // 저장을 위해 synchronize 함수를 호출해줍니다.
            sharedDefault?.synchronize()
        }
    }
}

