//
//  TodayViewController.swift
//  Widget
//
//  Created by yagom on 2015. 4. 3..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    
    // 화면에 IBOutlet으로 연결되는 레이블
    @IBOutlet weak var label: UILabel!
    
    // 시스템에서 위젯 화면을 업데이트 하려고 할 때 자동으로 호출됩니다.
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        
        // App group의 User Default를 로드합니다.
        let sharedDefault = NSUserDefaults(suiteName: "group.yagomWidget")
        
        // myText라는 키를 가진 문자열을 레이블의 텍스트로 지정해줍니다.
        self.label.text = sharedDefault?.objectForKey("myText") as? String
        
        // 정보 업데이트 도중 에러가 발생하면 NCUpdateResult.Failed를,
        // 업데이트가 필요없다면 NCUpdateResult.NoData를,
        // 업데이트가 되었다면 NCUpdateResult.NewData를 인자로 호출해줍니다.
        completionHandler(NCUpdateResult.NewData)
    }
    
}
