//
//  FileProvider.swift
//  DocFileProvider
//
//  Created by yagom on 2015. 4. 15..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

class FileProvider: NSFileProviderExtension {

    var fileCoordinator: NSFileCoordinator {
        let fileCoordinator = NSFileCoordinator()
        fileCoordinator.purposeIdentifier = self.providerIdentifier()
        return fileCoordinator
    }

    override init() {
        super.init()
        
        self.fileCoordinator.coordinateWritingItemAtURL(self.documentStorageURL(), options: NSFileCoordinatorWritingOptions(), error: nil, byAccessor: { newURL in
            // documentStorageURL이 실제로 존재하도록 만들어줍니다.
            var error: NSError? = nil
            NSFileManager.defaultManager().createDirectoryAtURL(newURL, withIntermediateDirectories: true, attributes: nil, error: &error)
        })
    }

    // Placeholder URL을 제공할 때 호출됩니다.
    override func providePlaceholderAtURL(url: NSURL, completionHandler: ((error: NSError?) -> Void)?) {
        // writePlaceholderAtURL(_:withMetadata:error:)을 placeholder URL과 함께 호출해 준 후, 에러가 수용할 수 있는 범위라면 completion handler를 호출합니다.
        let fileName = url.lastPathComponent!
    
        let placeholderURL = NSFileProviderExtension.placeholderURLForURL(self.documentStorageURL().URLByAppendingPathComponent(fileName))
    
        // TODO: url에 있는 파일의 사이즈를 구합니다
        let fileSize = 0
        let metadata = [NSURLFileSizeKey: fileSize]
        NSFileProviderExtension.writePlaceholderAtURL(placeholderURL, withMetadata: metadata, error: nil)

        completionHandler?(error: nil)
    }

    // URL에 위치한 파일을 제공하기 시작할 때 호출됩니다.
    override func startProvidingItemAtURL(url: NSURL, completionHandler: ((error: NSError?) -> Void)?) {
        // URLForItemWithIdentifier을 이용하여 해당 위치에 파일이 존재하는지 확인한 후, completion handler를 호출합니다.

        var fileError: NSError? = nil
      
        let fileData = NSData()

        _ = fileData.writeToURL(url, options: NSDataWritingOptions(), error: &fileError)
        
        completionHandler?(error: nil);
    }

    
    // File이 변경되었을 때 호출됩니다.
    override func itemChangedAtURL(url: NSURL) {
        // TODO: 적절한 업데이트 작업을 해줍니다.
        
        NSLog("Item changed at URL %@", url)
    }

    // 마지막 파일요청 뒤에 호출됩니다.
    override func stopProvidingItemAtURL(url: NSURL) {
        // 컨텐츠 파일을 삭제하기 안전한 포인트입니다.

        _ = NSFileManager.defaultManager().removeItemAtURL(url, error: nil)
        self.providePlaceholderAtURL(url, completionHandler: nil)
    }

}
