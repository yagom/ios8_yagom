//
//  DocumentPickerViewController.swift
//  Doc
//
//  Created by yagom on 2015. 4. 15..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

class DocumentPickerViewController: UIDocumentPickerExtensionViewController {

    // Import image.png 버튼을 눌렀을 때 호출되는 IBAction
    @IBAction func openDocument(sender: AnyObject?) {
        
        var returnURL: NSURL? = nil
        
        // Import 모드일 때에만 동작하도록 설계되었습니다.
        if self.documentPickerMode == UIDocumentPickerMode.Import {
            
            // Storage URL이 있으면
            if let url = self.documentStorageURL {
                // Storage내의 image.png 라는 파일의 URL을 만들어줍니다.
                let fileURL = self.documentStorageURL.URLByAppendingPathComponent("image.png")
                returnURL = fileURL
                println("\(fileURL)")
            }
        } else {
            println("설정이 잘못된 것 같습니다. Mode가 \(self.documentPickerMode.rawValue)입니다.")
        }
        self.dismissGrantingAccessToURL(returnURL)
    }

    // 진입할 때 호출되는 함수입니다.
    // Mode에 따라 화면을 적절한 화면을 구성하여줍니다.
    override func prepareForPresentationInMode(mode: UIDocumentPickerMode) {
        
        switch mode {
        case UIDocumentPickerMode.ExportToService:
            println("Export")
        case UIDocumentPickerMode.Import:
            println("Import")
        case UIDocumentPickerMode.MoveToService:
            println("Move")
        case UIDocumentPickerMode.Open:
            println("Open")
        }
    }
}
