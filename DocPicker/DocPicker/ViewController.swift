//
//  ViewController.swift
//  DocPicker
//
//  Created by yagom on 2015. 4. 15..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

// Uniform Type Identifiers를 사용하기 위해 MobileCoreServices import
import MobileCoreServices

class ViewController: UIViewController, UIDocumentPickerDelegate, UIDocumentMenuDelegate {

    // 파일 타입은 텍스트 파일과 이미지 파일로 한정합니다.
    let docTypes = [kUTTypeText, kUTTypeImage]
    
    //MARK:- Actions
    @IBAction func clickImportButton(sender: AnyObject) {
        
        // 사용가능한 Document Picker 메뉴들을 보여줍니다.
        let importMenu = UIDocumentMenuViewController(documentTypes: self.docTypes, inMode: UIDocumentPickerMode.Import)
        
        importMenu.delegate = self
        
        self.presentViewController(importMenu, animated: true, completion: nil)
        
        // 만약 Document Picker Menu를 보여주지 않고, 기본 Document Picker를 보여주고 싶다면 바로 생성해 호출해도 됩니다.
        /*
        let importPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeText, kUTTypeImage], inMode: UIDocumentPickerMode.Import)
        
        self.presentViewController(importPicker, animated: true, completion: nil)
        */
    }
    
    @IBAction func clickExportButton(sender: AnyObject) {

        // image.png를 업로드 할 폴더를 지정하는 Document Picker 뷰 컨트롤러를 보여줍니다.
        let exportPicker = UIDocumentPickerViewController(URL: NSBundle.mainBundle().URLForResource("image", withExtension: "png")!, inMode: UIDocumentPickerMode.ExportToService)
        
        exportPicker.delegate = self
        
        self.presentViewController(exportPicker, animated: true, completion: nil)
    }

    //MARK:- Document Menu Delegate
    // Document Picker 메뉴의 항목을 선택했을 때 호출되는 함수
    func documentMenu(documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            // 선택된 도큐먼트 피커를 화면에 보여줍니다.
            documentPicker.delegate = self
            self.presentViewController(documentPicker, animated: true, completion: nil)
    }
    
    //MARK:- Document Picker Delegate
    // Import 도큐먼트 피커에서 파일이 선택되었을 때
    func documentPicker(controller: UIDocumentPickerViewController, didPickDocumentAtURL url: NSURL) {

        // 도큐먼트 가져오기 모드일 때
        if controller.documentPickerMode == UIDocumentPickerMode.Import {
            
            // 가져온 파일에 관한 정보를 Alert를 통해 보여줍니다.
            let alert: UIAlertController = UIAlertController(title: "성공", message: "\(url.lastPathComponent)", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "확인", style: UIAlertActionStyle.Cancel, handler: nil))
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.presentViewController(alert, animated: true, completion: nil)
            })
            
            // 가져온 파일을 어떻게 하고싶은지 적절히 처리합니다.
        }
    }
}

