//
//  ViewController.swift
//  Handoff
//
//  Created by yagom on 2015. 4. 11..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, NSUserActivityDelegate {
    
    // 텍스트를 입력받을 필드
    @IBOutlet weak var textField: UITextField!
    
    // 테스트를 위해 시스템 상황을 화면에 보여주기 위한 텍스트뷰
    @IBOutlet weak var textView: UITextView!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 액티비티가 업데이트 되었다는 것을 알려주는 노티피케이션을 듣기 위해 옵저버로 등록합니다.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateField:", name: DidUpdateUserActivityNotification, object: nil)
        
        // 액티비티가 복원이 필요하다는 것을 알려주는 노티피케이션을 듣기 위해 옵저버로 등록합니다.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "restoreActivity:", name: ShouldRestoreUserActivityNotification, object: nil)
        
        // Info.plist 파일에 NSUserActivityTypes으로 넣어주었던 액티비티 타입으로 유저 액티비티를 만들어 줍니다.
        self.userActivity = NSUserActivity(activityType: SampleActivityType)
        self.userActivity?.title = "Sample"
        self.userActivity?.delegate = self
        
        // 유저가 현재 사용중임을 알립니다.
        self.userActivity?.becomeCurrent()
        
    }
    
    // 뷰컨트롤러 인스턴스가 메모리에서 해제될 때 호출됩니다.
    deinit {
        // 유저 액티비티를 더이상 사용할 수 없게 무효화 하고, 노티피케이션 센터 옵저버 등록을 해제합니다.
        self.userActivity?.invalidate()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK:- Custom Functions
    
    // 액티비티 복원을 위한 노티피케이션을 받는 함수
    @objc private func restoreActivity(noti: NSNotification) {
        if let activity = noti.userInfo?[UserInfoKeyActivity] as? NSUserActivity {
            self.restoreUserActivityState(activity)
        }
    }
    
    // 텍스트뷰에 텍스트를 계속 이어붙여 줍니다.
    private func appendToTextView(str: String?) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let ori = self.textView.text {
                var aps = ori.stringByAppendingFormat("\n\n%@", str!)
                self.textView.text = aps
            } else {
                self.textView.text = str
            }
        })
    }
    
    //MARK:- Update View
    @objc private func updateField(noti: NSNotification) {
        if let text = noti.userInfo?[ItemKey] as? String {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.textField.text = text
            })
        }
    }
    
    //MARK:- Text Field Delegate
    // 사용자가 텍스트 필드의 텍스트를 변경할 때 호출됩니다.
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        // 유저 액티비티가 업데이트가 필요함을 bool flag로 설정해 줍니다.
        // 당장 업데이트 되는 것은 아닙니다. 시스템이 알아서 필요할 때 처리합니다.
        self.userActivity?.needsSave = true
        
        return true
    }
    
    // 텍스트 필드의 키보드에서 return 키를 눌렀을 때 호출됩니다.
    func textFieldShouldReturn(textField: UITextField) -> Bool {

        // 키보드를 내려주고
        textField.endEditing(true)
        
        // 유저 액티비티 상태를 강제로 업데이트 해줍니다.
        self.updateUserActivityState(self.userActivity!)
        
        return true
    }
    
    //MARK:- Override Functions
    // 유저 액티비티 상태를 복원할 때 호출됩니다.
    override func restoreUserActivityState(activity: NSUserActivity) {
        // 복원된 액티비티를 지금의 액티비티로 설정해줍니다.
        self.userActivity = activity
        
        self.appendToTextView("액티비티 복원 \(activity.userInfo!.debugDescription)")
        super.restoreUserActivityState(activity)
    }
    
    // 액티비티를 업데이트 할 때 호출됩니다.
    override func updateUserActivityState(activity: NSUserActivity) {
        
        var text: String = ""
        
        // 텍스트 필드의 텍스트가 있다면 텍스트를 가져옵니다.
        if let str = self.textField.text {
            text = str
        }
        
        // 유저 액티비티의 유저정보로 아이템 키와 텍스트를 쌍으로 이룬 딕셔너리를 넣어줍니다.
        activity.addUserInfoEntriesFromDictionary([ItemKey:text])
        
        self.appendToTextView("액티비티 업데이트 \(activity.userInfo!.debugDescription)")
        
        super.updateUserActivityState(activity)
    }
    
    
    //MARK:- User Activity Delegate
    
    // 유저 액티비티가 저장되려 할 때 호출됩니다.
    func userActivityWillSave(userActivity: NSUserActivity) {
        self.appendToTextView("액티비티가 저장될 예정")
    }
    
    // 다른 기기에서 유저 액티비티가 이어지면 호출됩니다.
    func userActivityWasContinued(userActivity: NSUserActivity) {
        self.appendToTextView("다른 기기에서 접속")
    }
}

