//
//  Constants.swift
//  Handoff
//
//  Created by yagom on 2015. 4. 11..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import Foundation


let ItemKey = "Handoff.item.key"
let SampleActivityType = "net.yagom.handoff.sample"

let DidUpdateUserActivityNotification = "didUpdateUserActivity"
let ShouldRestoreUserActivityNotification = "restoreActivity"

let UserInfoKeyActivity = "activity"