//
//  AppDelegate.swift
//  Handoff
//
//  Created by yagom on 2015. 4. 11..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    // 유저 액티비티가 업데이트 되었을 때 호출됩니다.
    func application(application: UIApplication, didUpdateUserActivity userActivity: NSUserActivity) {
        
        // 액티비티가 업데이트 되었다는 것을 알려주는 노티피케이션을 발송합니다.
        NSNotificationCenter.defaultCenter().postNotificationName(DidUpdateUserActivityNotification, object: nil, userInfo: userActivity.userInfo)
    }
    
    // 핸드오프 유저 액티비티를 복원할 때, 즉, 왼쪽 아래 아이콘을 끌어올려 실행할 때 호출됩니다.
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]!) -> Void) -> Bool {
        
        println("핸드오프 유저 액티비티 복원을 위해 호출된 continueUserActivity: \(userActivity.userInfo)")
        
        // 액티비티가 복원이 필요하다는 것을 알려주는 노티피케이션을 발송합니다.
        NSNotificationCenter.defaultCenter().postNotificationName(ShouldRestoreUserActivityNotification, object: nil, userInfo: [UserInfoKeyActivity: userActivity])
        
        return true
    }
}

