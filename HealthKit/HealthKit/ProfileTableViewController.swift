//
//  ProfileTableViewController.swift
//  HealthKit
//
//  Created by yagom on 2015. 2. 2..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import HealthKit

// HKHealthStore 클래스 확장을 위해 extension을 사용합니다.
extension HKHealthStore {
    func mostRecentQuantitySampleOfType(quantityType: HKQuantityType, predicate: NSPredicate?, completion: ((HKQuantity?, NSError?) -> Void)!) {
        
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        
        let query: HKSampleQuery = HKSampleQuery(sampleType: quantityType, predicate: predicate, limit: 1, sortDescriptors: [descriptor]) { (query: HKSampleQuery!, results: [AnyObject]!, error: NSError!) -> Void in
            
            if results.count > 0 {
                if let completion = completion {
                    
                    let sample: HKQuantitySample = results.first as! HKQuantitySample
                    let quantity: HKQuantity = sample.quantity
                    
                    completion(quantity, error)
                }
            } else {
                if let completion = completion {
                    completion(nil, error)
                    return
                }
            }
        }
        self.executeQuery(query)
    }
}

class ProfileTableViewController: UITableViewController {
    
    @IBOutlet weak private var ageTitleLabel: UILabel!
    @IBOutlet weak private var heightTitleLabel: UILabel!
    @IBOutlet weak private var weightTitleLabel: UILabel!
    
    @IBOutlet weak private var ageValueLabel: UILabel!
    @IBOutlet weak private var heightValueLabel: UILabel!
    @IBOutlet weak private var weightValueLabel: UILabel!
    
    
    private enum ProfileTableCellIndex: Int {
        case Age = 0
        case Height = 1
        case Weight = 2
    }
    
    private var healthStore: HKHealthStore = HKHealthStore()
    
    // MARK: - Life cycle
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if HKHealthStore.isHealthDataAvailable() {
            
            let writeDataTypes = self.dataTypesToWrite()
            let readDataTypes = self.dataTypesToRead()

            // HealthStore에 읽기, 쓰기권한을 요청합니다.
            self.healthStore.requestAuthorizationToShareTypes(writeDataTypes, readTypes: readDataTypes, completion: { (success: Bool, error: NSError!) -> Void in
                if success == false {
                    println("error \(error)")
                    
                    return
                }
                
                // UI 변화는 메인스레드에서 진행해 주어야 합니다.
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.updateAgeLabel()
                    self.updateHeightLabel()
                    self.updateWeightLabel()
                })
            })
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var edgeInset: UIEdgeInsets = UIEdgeInsetsZero
        edgeInset.top = UIApplication.sharedApplication().statusBarFrame.size.height
        
        self.tableView.contentInset = edgeInset
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    // MARK: - Permissions
    
    // 총 에너지 사용량, 활성 에너지 사용량, 키, 몸무게 등의 쓰기권한을 얻기 위한 타입 셋을 반환합니다.
    private func dataTypesToWrite() -> Set<NSObject> {
        let dietaryCalorieEnergyType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)
        let activeEnergyBurnType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)
        let heightType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        let weightType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        
        return Set<NSObject>(arrayLiteral: dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType)
    }
    
    // 총 에너지 사용량, 활성 에너지 사용량, 키, 몸무게, 생일, 성별 등의 읽기권한을 얻기 위한 타입 셋을 반환합니다.
    private func dataTypesToRead() -> Set<NSObject> {
        let dietaryCalorieEnergyType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)
        let activeEnergyBurnType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)
        let heightType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        let weightType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        let birthdayType: HKCharacteristicType = HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)
        let biologicalSexType: HKCharacteristicType = HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)
        
        return Set<NSObject>(arrayLiteral: dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType, birthdayType, biologicalSexType)
    }
    
    // MARK :- Update Label
    
    // 나이 레이블을 업데이트 합니다.
    private func updateAgeLabel() {
        self.ageTitleLabel.text = "Age (yrs)"
        
        var error: NSError?
        let dateOfBirth: NSDate? = self.healthStore.dateOfBirthWithError(&error)
        
        if let birth = dateOfBirth {
            
            let ageComponents = NSCalendar.currentCalendar().components(NSCalendarUnit.YearCalendarUnit, fromDate: birth, toDate: NSDate(), options: NSCalendarOptions.WrapComponents)
            
            let age = ageComponents.year as Int
            
            self.ageValueLabel.text = "\(age)"
            
        } else {
            println("fetching age data fail \(error?.localizedDescription)");
            self.ageValueLabel.text = "Not Available"
        }
    }
    
    // 신장 레이블을 업데이트 합니다.
    private func updateHeightLabel() {
        let lengthFormatter = NSLengthFormatter()
        lengthFormatter.unitStyle = NSFormattingUnitStyle.Long
        
        let heightFormatterUnit: NSLengthFormatterUnit = NSLengthFormatterUnit.Centimeter
        let heightUnitString = lengthFormatter.unitStringFromValue(10, unit: heightFormatterUnit) as String

        self.heightTitleLabel.text = "Height (\(heightUnitString))"
        
        let heightType: HKQuantityType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        
        self.healthStore.mostRecentQuantitySampleOfType(heightType, predicate: nil) { (mostRecentQuantity: HKQuantity?, error: NSError?) -> Void in
            
            if let quantity = mostRecentQuantity {
                let heightUnit = HKUnit.meterUnit()
                let usersHeight = quantity.doubleValueForUnit(heightUnit) * 100.0

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.heightValueLabel.text = "\(usersHeight)"
                })
            } else {
                println("fetching height data fail \(error?.localizedDescription)");
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.heightValueLabel.text = "Not Available"
                })
            }
        }
    }
    
    // 체중 레이블을 업데이트 합니다.
    private func updateWeightLabel() {
        let massFormatter: NSMassFormatter = NSMassFormatter()
        massFormatter.unitStyle = NSFormattingUnitStyle.Long
        
        let weightFormatterUnit = NSMassFormatterUnit.Kilogram
        let weightUnitString = massFormatter.unitStringFromValue(10, unit: weightFormatterUnit)
        
        self.weightTitleLabel.text = "Weight (\(weightUnitString))"
        
        let weightType: HKQuantityType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        
        self.healthStore.mostRecentQuantitySampleOfType(weightType, predicate: nil) { (mostRecentQuantity: HKQuantity?, error: NSError?) -> Void in
            
            if let quantity = mostRecentQuantity {
                let weightUnit = HKUnit.gramUnitWithMetricPrefix(HKMetricPrefix.Kilo)
                let usersWeight = quantity.doubleValueForUnit(weightUnit)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.weightValueLabel.text = "\(usersWeight)"
                })
            } else {
                println("fetching weight data fail \(error?.localizedDescription)");
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.weightValueLabel.text = "Not Available"
                })
            }
        }

    }
    
    // MARK: - Update Information
    
    // 신장을 HealthStore에 저장합니다.
    @objc private func saveHeightToHealthStore(height: Double) {
        let meterUnit: HKUnit = HKUnit.meterUnit()
        let quantity: HKQuantity = HKQuantity(unit: meterUnit, doubleValue:height / 100.0)
        
        let heightType: HKQuantityType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        let now = NSDate()
        
        let heightSample: HKQuantitySample = HKQuantitySample(type: heightType, quantity: quantity, startDate: now, endDate: now)
        
        self.healthStore.saveObject(heightSample, withCompletion: { (success: Bool, error: NSError!) -> Void in
            if success == false {
                println("Save height fail \(error.localizedDescription)")
            } else {
                self.updateHeightLabel()
            }
        })
    }
    
    // 체중을 HealthStore에 저장합니다.
    @objc private func saveWeightToHealthStore(weight: Double) {
        let kiloGramUnit: HKUnit = HKUnit.gramUnitWithMetricPrefix(HKMetricPrefix.Kilo)
        let quantity: HKQuantity = HKQuantity(unit: kiloGramUnit, doubleValue:weight)
        
        let weightType: HKQuantityType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        let now = NSDate()
        
        let weightSample: HKQuantitySample = HKQuantitySample(type: weightType, quantity: quantity, startDate: now, endDate: now)
        
        self.healthStore.saveObject(weightSample, withCompletion: { (success: Bool, error: NSError!) -> Void in
            if success == false {
                println("Save weight fail \(error.localizedDescription)")
            } else {
                self.updateWeightLabel()
            }
        })
    }
    
    // MARK: - Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var alert: UIAlertController!
        
        if indexPath.row == ProfileTableCellIndex.Age.rawValue {
            
            alert = UIAlertController(title: "Inform", message: "Can not change date of birth", preferredStyle: UIAlertControllerStyle.Alert)
            
            let action: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) -> Void in
                alert.dismissViewControllerAnimated(true, completion: nil)
            })
            
            alert.addAction(action)
            
            self.presentViewController(alert, animated: true, completion: nil)
            
            return
        }
        
        var alertTitle: String!
        
        switch indexPath.row {
        case ProfileTableCellIndex.Height.rawValue:
            alertTitle = "Enter your height";
            
        case ProfileTableCellIndex.Weight.rawValue:
            alertTitle = "Enter your weight";
            
        default:
            alertTitle = nil
        }
        
        
        alert = UIAlertController(title: alertTitle, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        
        // 얼럿창에 데이터를 입력받을 수 있는 텍스트 필드를 추가해주고 키보드 타입을 숫자 패드로 변경해줍니다.
        alert.addTextFieldWithConfigurationHandler({ (textField: UITextField!) -> Void in
            textField.keyboardType = UIKeyboardType.DecimalPad
        })
        
        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
            if let field: UITextField = alert.textFields?.first as? UITextField {
                let value: Double = (field.text as NSString).doubleValue
                
                switch indexPath.row {
                case ProfileTableCellIndex.Height.rawValue:
                    self.saveHeightToHealthStore(value)
                    
                case ProfileTableCellIndex.Weight.rawValue:
                    self.saveWeightToHealthStore(value)
                    
                default:
                    break
                }
            }
        })
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
