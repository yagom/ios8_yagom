//
//  EnergyTableViewController.swift
//  HealthKit
//
//  Created by yagom on 2015. 2. 3..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import HealthKit

class EnergyTableViewController: UITableViewController {
    
    @IBOutlet weak private var activeEnergyValueLabel: UILabel!
    @IBOutlet weak private var restingEnergyValueLabel: UILabel!
    @IBOutlet weak private var consumedEnergyValueLabel: UILabel!
    @IBOutlet weak private var netEnergyValueLabel: UILabel!
    
    private lazy var healthStore: HKHealthStore = HKHealthStore()
    
    // 프로퍼티(속성) 대부분 저장속성이 아닌 계산속성으로 처리하고 있습니다.
    private var energyFormatter: NSEnergyFormatter {
        get {
            let formatter = NSEnergyFormatter()
            formatter.unitStyle = NSFormattingUnitStyle.Long
            formatter.forFoodEnergyUse = true
            formatter.numberFormatter.maximumFractionDigits = 2
            
            return formatter
        }
    }
    
    private var activeEnergyBurned: Double {
        get {
            if let string: NSString = self.activeEnergyValueLabel.text {
                return string.doubleValue
            }
            return 0.0
        }
        
        set (newEnergy) {
            self.activeEnergyValueLabel.text = self.energyFormatter.stringFromValue(newEnergy, unit: NSEnergyFormatterUnit.Kilocalorie)
        }
    }
    
    
    private var restingEnergyBurned: Double {
        get {
            if let string: NSString = self.restingEnergyValueLabel.text {
                return string.doubleValue
            }
            return 0.0
        }
        
        set (newEnergy) {
            self.restingEnergyValueLabel.text = self.energyFormatter.stringFromValue(newEnergy, unit: NSEnergyFormatterUnit.Kilocalorie)
        }
    }
    
    
    private var energyConsumed: Double {
        get {
            if let string: NSString = self.consumedEnergyValueLabel.text {
                return string.doubleValue
            }
            return 0.0
        }
        
        set (newEnergy) {
            self.consumedEnergyValueLabel.text = self.energyFormatter.stringFromValue(newEnergy, unit: NSEnergyFormatterUnit.Kilocalorie)
        }
    }
    
    private var netEnergy: Double {
        get {
            if let string: NSString = self.netEnergyValueLabel.text {
                return string.doubleValue
            }
            return 0.0
        }
        
        set (newEnergy) {
            self.netEnergyValueLabel.text = self.energyFormatter.stringFromValue(newEnergy, unit: NSEnergyFormatterUnit.Kilocalorie)
        }
    }
    
    private var predicateForSamplesToday: NSPredicate {
        get {
            let calendar = NSCalendar.currentCalendar()
            let now = NSDate()
            
            let startDate = calendar.startOfDayForDate(now)
            let endDate = calendar.dateByAddingUnit(NSCalendarUnit.DayCalendarUnit, value: 1, toDate: startDate, options: NSCalendarOptions.allZeros)
            
            return HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: HKQueryOptions.StrictStartDate)
        }
    }
    
    
    // MARK: - Life Cycle
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var edgeInset: UIEdgeInsets = UIEdgeInsetsZero
        edgeInset.top = UIApplication.sharedApplication().statusBarFrame.size.height
        
        self.tableView.contentInset = edgeInset
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refreshControl?.addTarget(self, action: "refreshCells", forControlEvents: UIControlEvents.ValueChanged)
        
        self.refreshCells()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"refreshCells", name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    // swift에서는 dealloc 대신 deinit이라는 함수를 사용합니다
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    // MARK: - Reading Health Data
    
    // Selector를 이용한 동적 함수 할당을 위해 @objc 속성을 사용해야 합니다.
    @objc private func refreshCells() {
        self.refreshControl?.beginRefreshing()
        
        let energyConsumedType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)

        let activeEnergyBurnType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)
        
        self.fetchSumOfSamplesTodayForType(activeEnergyBurnType, unit: HKUnit.kilocalorieUnit()) { (activeEnergyBurned: Double?, error: NSError?) -> Void in
            
            if let activeBurned = activeEnergyBurned {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.activeEnergyBurned = activeBurned;
                    })
                
                self.fetchTotalBasalBurn({ (basalEnergyBurn: HKQuantity?, error: NSError?) -> Void in
                    
                    if let basalBurn = basalEnergyBurn?.doubleValueForUnit(HKUnit.kilocalorieUnit()) {
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.restingEnergyBurned = basalBurn
                        })
                        
                        self.fetchSumOfSamplesTodayForType(energyConsumedType, unit: HKUnit.kilocalorieUnit()) { (totalKCalConsumed: Double?, error: NSError?) -> Void in
                            
                            if let totalConsumed = totalKCalConsumed {
                                
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    
                                    self.energyConsumed = totalConsumed
                                    
                                    self.netEnergy = totalConsumed - activeBurned - basalBurn
                                    
                                    self.refreshControl?.endRefreshing()
                                })
                            } else {
                                println("An error occurred trying to fetch total consumed calories \(error)")
                            }
                        }
                    } else {
                        println("An error occurred trying to compute the basal energy burn \(error)")
                    }
                })
            } else {
                println("An error occurred trying to fetch active burned energy \(error)")
            }
        }
    }
    
    // 주어진 타입의 오늘 샘플량을 가져옵니다.
    private func fetchSumOfSamplesTodayForType(quantityType: HKQuantityType, unit: HKUnit, completion: ((Double?, NSError?) -> Void)!) {
        
        let query: HKStatisticsQuery = HKStatisticsQuery(quantityType: quantityType, quantitySamplePredicate: self.predicateForSamplesToday, options: HKStatisticsOptions.CumulativeSum) { (query: HKStatisticsQuery!, result: HKStatistics!, error: NSError!) -> Void in
            
            if let sum: HKQuantity = result.sumQuantity() {
                
                if let completion = completion {
                    let value: Double = sum.doubleValueForUnit(unit)
                    completion(value, error)
                }
            } else {
                completion(nil, error)
                return
            }
        }
        self.healthStore.executeQuery(query)
    }

    // BMR 지수를 체중과 신장, 나이 그리고 성별을 통해 계산합니다.
    private func calculateBMRFromWeight(weightInKilograms: Double, heightInCentimeters: Double, age: Int, biologicalSex: HKBiologicalSex) -> Double {
        
        var bmr: Double
        
        if biologicalSex == HKBiologicalSex.Male {
            bmr = 66.0 + (13.8 * weightInKilograms) + (5.0 * heightInCentimeters) - (6.8 * Double(age))
        } else {
            bmr = 65.5 + (9.6 * weightInKilograms) + (1.8 * heightInCentimeters) - (4.7 * Double(age))
        }
        
        return bmr
    }
    
    // 오늘 남은 열랑을 구합니다.
    private func calculateBasalBurnTodayFromWeight(weight: HKQuantity?, height: HKQuantity?, dateOfBirth: NSDate?, biologicalSex: HKBiologicalSexObject?) -> HKQuantity? {
        
        if weight == nil || height == nil || dateOfBirth == nil || biologicalSex == nil {
            return nil
        }
        
        let heightInCentimeter = height?.doubleValueForUnit(HKUnit(fromString: "cm"))
        let weightInKilogram = weight?.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(HKMetricPrefix.Kilo))
        
        let now = NSDate()
        let ageComponents = NSCalendar.currentCalendar().components(NSCalendarUnit.YearCalendarUnit, fromDate: dateOfBirth!, toDate: now, options: NSCalendarOptions.WrapComponents)
        
        let age = ageComponents.year
        
        let bmr = self.calculateBMRFromWeight(weightInKilogram!, heightInCentimeters: heightInCentimeter!, age: age, biologicalSex: biologicalSex!.biologicalSex)
        
        let startOfToday = NSCalendar.currentCalendar().startOfDayForDate(now)
        let endOfToday = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.DayCalendarUnit, value: 1, toDate: startOfToday, options: NSCalendarOptions.allZeros)
        
        let secondsInDay = endOfToday?.timeIntervalSinceDate(startOfToday)
        
        let percentOfDayComplete = now.timeIntervalSinceDate(startOfToday) / secondsInDay!
        
        let kilocaloriesBurned = bmr * percentOfDayComplete
        
        return HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: kilocaloriesBurned)
    }
    
    // 오늘 소비해야 하는 열량을 구합니다.
    private func fetchTotalBasalBurn(completion: ((HKQuantity?, NSError?) -> Void)!) {
        let weightType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        let heightType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        
        self.healthStore.mostRecentQuantitySampleOfType(weightType, predicate: nil) { (weight: HKQuantity?, error: NSError?) -> Void in
            
            if let weight = weight {
                
                self.healthStore.mostRecentQuantitySampleOfType(heightType, predicate: nil, completion: { (height: HKQuantity?, error: NSError?) -> Void in
                    
                    if let height = height {
                        
                        if let dateOfBirth =  self.healthStore.dateOfBirthWithError(nil) {
                            
                            if let biologicalSexObject = self.healthStore.biologicalSexWithError(nil) {
                                
                                let basalEnergyBurn = self.calculateBasalBurnTodayFromWeight(weight, height: height, dateOfBirth: dateOfBirth, biologicalSex: biologicalSexObject)
                             
                                completion(basalEnergyBurn, nil)
                                
                            } else {
                                completion(nil, error)
                                return
                            }
                        } else {
                            completion(nil, error)
                            return
                        }
                        
                    } else {
                        completion(nil, error)
                        return
                    }
                })
                
            } else {
                completion(nil, error)
                return
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
}
