//
//  PhotoCollectionViewCell.swift
//  PhotoKit
//
//  Created by yagom on 2015. 1. 20..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
