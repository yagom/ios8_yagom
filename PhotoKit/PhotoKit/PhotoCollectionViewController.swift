//
//  PhotoCollectionViewController.swift
//  PhotoKit
//
//  Created by yagom on 2015. 1. 20..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

// PhotoKit 사용을 위하여 import 한다.
import Photos

// UICollectionViewCell을 사용하기 위하여 스토리보드에 등록한 Reuse Identifier를 똑같이 사용한다.
let reuseIdentifier = "PhotoCell"

// PhotoCollectionViewController는 UICollectionViewController를 상속받았으며
// PHPhotoLibraryChangeOberver 프로토콜을 따를 것이고,
// UICollectionViewDelegateFlowLayout 프로토콜을 따를 것이다.
class PhotoCollectionViewController: UICollectionViewController, PHPhotoLibraryChangeObserver, UICollectionViewDelegateFlowLayout {
    
    // 외부로 보여지고 싶지 않은 변수들은 private 키워드를 사용하여 외부에서 접근을 차단한다.
    
    // Asset 가져오기 결과
    private var assetsFetchResult: PHFetchResult?
    // 캐시 이미지 매니져
    private var cachingImageManager: PHCachingImageManager = PHCachingImageManager()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // 사진 Asset을 가져온다.
        self.assetsFetchResult = PHAsset.fetchAssetsWithOptions(nil)
        
        // 포토라이브러리가 변경되면 델리게이트 메소드를 호출받을 수 있도록 Photo Library Change Obsever로 등록한다.
        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)
    }

    // MARK: - Collection View Layout Flow
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        // 콜렉션뷰 셀의 가로 및 세로 사이즈를 반환한다.
        if let viewWidth = self.collectionView?.frame.size.width {
            return CGSizeMake((viewWidth - 50) / 4.0, (viewWidth - 50) / 4.0)
        }
        
        return CGSizeZero
    }

    // MARK: - UICollectionViewDataSource
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // 섹션은 1개
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // 가져온 Asset에 아이템이 있으면 아이템 개수만큼 셀 개수를 반환
        if let count = self.assetsFetchResult?.count {
            return count
        }
        
        // Asset을 가져오지 못했다면 셀은 0개다.
        return 0
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // 스토리보드에 미리 구현해둔 셀을 가져온다.
        var cell: PhotoCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PhotoCollectionViewCell

        // index에 맞는 Asset을 가져온다.
        if let asset: PHAsset = self.assetsFetchResult?.objectAtIndex(indexPath.row) as? PHAsset{
            
            // 현재 기기 화면의 스케일
            let scale: CGFloat = UIScreen.mainScreen().scale
            
            // 현재 콜렉션 뷰 컨트롤러의 레이아웃을 가져온다.
            let flowLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
            
            // 현재 레이아웃의 셀 사이즈
            let cellSize: CGSize = flowLayout.itemSize
            
            // Image Manager를 통해서 Size에 맞게 이미지를 가져온다.
            self.cachingImageManager.requestImageForAsset(asset, targetSize: CGSizeMake(cellSize.width * scale, cellSize.height * scale), contentMode: PHImageContentMode.AspectFill, options: nil, resultHandler: { (result: UIImage!, info: [NSObject:AnyObject]!) -> Void in
                // 가져온 이미지를 셀의 image view에 세팅한다.
                cell.imageView.image = result
            })
        }
        return cell
    }

    // MARK: - PHPhotoLibraryChangeObserver
    // 포토 라이브러리가 변경사항이 생기면 호출되는 메소드이다.
    func photoLibraryDidChange(changeInstance: PHChange!) {
        dispatch_async(dispatch_get_main_queue(), {
            // 다시 Asset을 받아온다
            self.assetsFetchResult = PHAsset.fetchAssetsWithOptions(nil)
            // 콜렉션뷰를 새로고침 한다.
            self.collectionView?.reloadData()
        })
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // 선택한 콜렉션셀의 인덱스 값을 가져와서
        if let index: NSIndexPath = self.collectionView?.indexPathForCell(sender as! UICollectionViewCell) {
            // 이동할 뷰컨트롤러를 가져온 후
            let nextViewController: PhotoViewController = segue.destinationViewController as! PhotoViewController
            // 해당 이미지 에셋을 전달해준다.
            nextViewController.asset = self.assetsFetchResult?.objectAtIndex(index.item) as? PHAsset
        }
    }
}
