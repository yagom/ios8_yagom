//
//  PhotoViewController.swift
//  PhotoKit
//
//  Created by yagom on 2015. 1. 22..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import Photos

class PhotoViewController: UIViewController, PHPhotoLibraryChangeObserver {

    // 현재 화면에 표시될 사진의 Asset
    var asset: PHAsset?
    
    // 외부로 보여지고 싶지 않은 변수들은 private 키워드를 사용하여 외부에서 접근을 차단한다.
    
    // 사진보정 포멧ID로 쓰일 문자열이다. 기본적으로는 번들ID를 사용하는 것이 좋다.
    private let AdjustmentFormatIdentifier: String = "net.yagom.PhotoKit"
    
    // 스토리보드의 Image View와 아울렛 연결되는 Image View 변수
    @IBOutlet private weak var imageView: UIImageView!
    
    // lazy 키워드를 사용하면 사용 처음 불려지는 시점에서 객체를 할당하게 된다.
    // 지금은 굳이 꼭 필요하진 않지만, 예시를 위해 사용해본다.
    
    // 이미지 매니져
    private lazy var defaultImageManager: PHImageManager = PHImageManager.defaultManager()
    // OpenGL을 사용하기 위한 랜더릴 컨텍스트
    private lazy var eaglContext: EAGLContext? = EAGLContext(API: EAGLRenderingAPI.OpenGLES2)
    // EAGL 컨텍스트를 사용하는 CIContext
    private lazy var ciContext: CIContext? = CIContext(EAGLContext: self.eaglContext)
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // 사진 라이브러리가 변경되면 알 수 있도록 옵저버 등록을 해둔다
        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        // 처음 화면이 보여질 때 이미지를 가져올 수 있도록 업데이트 한다.
        self.updateImage()
    }
    
    deinit {
        // 뷰컨트롤러가 메모리에서 해제될 때 오버저 등록을 해제해 줘야한다.
        PHPhotoLibrary.sharedPhotoLibrary().unregisterChangeObserver(self)
    }
    
    
    // MARK: - Image Adjustment
    
    // 에셋의 이미지를 가져와서 화면에 보여준다.
    func updateImage() {
        if let currentImageAsset = self.asset {
            
            // 현재 화면의 배율
            let scale: CGFloat = UIScreen.mainScreen().scale
            // 이미지를 가져올 때 사용할 크기
            let size: CGSize = CGSizeMake(self.imageView.bounds.width * scale, self.imageView.bounds.height * scale)
            
            // 이미지를 가져올 때 사용할 옵션
            var options: PHImageRequestOptions = PHImageRequestOptions()
            
            // iCloud 이미지를 가져올 수도 있으므로 네트워크 사용을 허용한다.
            options.networkAccessAllowed = true
            
            // 현재 이미지 에셋과 이미지 뷰 사이즈, 옵션 등을 이용하여 이미지를 가져온다.
            self.defaultImageManager.requestImageForAsset(currentImageAsset, targetSize: size, contentMode: PHImageContentMode.AspectFill, options: options, resultHandler: { (image: UIImage!, info: [NSObject:AnyObject]!) -> Void in
                
                dispatch_async(dispatch_get_main_queue(), {
                    // 가져온 이미지는 화면의 이미지뷰에 세팅해준다.
                    self.imageView.image = image
                })
            })
        }
    }
    
    // 이미지에 필터를 적용한다. 더 많은 정보를 확인하려면 아래 링크를 따라가 정보를 볼 수 있다.
    // https://developer.apple.com/library/ios/documentation/GraphicsImaging/Reference/CoreImageFilterReference/
    func applyFilter(name: String, optionValue:[String:AnyObject]? = nil) -> Void {
        
        // 이미지 수정 요청 옵션을 생성한다.
        let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
        options.canHandleAdjustmentData = { (adjustmentData: PHAdjustmentData!) -> Bool in
            return (adjustmentData.formatIdentifier == self.AdjustmentFormatIdentifier && adjustmentData.formatVersion == "1.0") == true
        }
        
        // 컨텐츠 수정을 요청한다.
        self.asset?.requestContentEditingInputWithOptions(options, completionHandler: { (contentEditingInput: PHContentEditingInput!, info: [NSObject:AnyObject]!) -> Void in
            
            // 수정할 이미지의 URL
            let url: NSURL = contentEditingInput.fullSizeImageURL

            // 이미지의 가로세로 회전상태 Flag
            let orientation: Int32 = contentEditingInput.fullSizeImageOrientation
            
            // URL을 통하여 수정할 이미지를 가져온다
            var inputImage: CIImage = CIImage(contentsOfURL: url)
            
            // 회전상태를 반영
            inputImage = inputImage.imageByApplyingOrientation(orientation)
            
            // 이미지에 적용할 CIFilter 생성
            let filter: CIFilter = CIFilter(name: name)
            filter.setDefaults()
            
            // 필터를 적용할 이미지를 세팅
            filter.setValue(inputImage, forKey: kCIInputImageKey)
            
            // 추가적인 옵션사항이 있다면
            if let options = optionValue {
                for (key, value) in options {
                    // 필터에 옵션값을 적용해준다.
                    filter.setValue(value, forKey: key)
                }
            }
            
            // 필터를 적용한 이미지 결과물
            let outputImage: CIImage = filter.outputImage
            var jpegData: NSData? = nil
            
            // 적용된 이미지 결과물을 CGImage로 그려준다.
            if let outputImageRef: CGImageRef = self.ciContext?.createCGImage(outputImage, fromRect: outputImage.extent()) {
                // UIImage 객체를 CGImage로 그려진 이미지를 통해 생성한다
                if let img: UIImage = UIImage(CGImage: outputImageRef, scale: 1.0, orientation: UIImageOrientation.Up) {
                    // 실질적으로 저장할 JPEG 이미지 데이터를 생성한다
                    jpegData = UIImageJPEGRepresentation(img, 1.0)
                }
            }
            
            // 사진보정 데이터를 생성한다.
            let adjustmentData: PHAdjustmentData = PHAdjustmentData(formatIdentifier: self.AdjustmentFormatIdentifier, formatVersion: "1.0", data: name.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true))
            
            // 컨텐츠 수정 요청물을 통해 컨텐츠 수정 결과물을 생성한다.
            let contentEditingOutput: PHContentEditingOutput = PHContentEditingOutput(contentEditingInput: contentEditingInput)
            
            // JPEG 데이터를 컨텐츠 수정 결과 URL에 저장한다.
            jpegData?.writeToURL(contentEditingOutput.renderedContentURL, atomically: true)
            contentEditingOutput.adjustmentData = adjustmentData
            
            // 사진 라이브러리에 수정사항 반영을 요청한다.
            PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
                let request: PHAssetChangeRequest = PHAssetChangeRequest(forAsset: self.asset)
                request.contentEditingOutput = contentEditingOutput
                }, completionHandler: { (success: Bool, error: NSError!) -> Void in
                    println("Change request result \nSuccess : \(success)\nError: \(error)")
            })
            
        })
    }
    
    // MARK: - IBActions
    
    // EDIT 버튼을 눌렀을 때 호출되는 메소드
    @IBAction func clickEditButton(sender: UINavigationItem) {
        
        // ActionSheet로 사용할 UIAlertController 객체를 생성한다.
        var alert: UIAlertController = UIAlertController(title:"보정", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        // 취소 버튼을 먼저 추가해준다.
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        
        
        // Asset이 컨텐츠를 수정할 수 있는지 체크한다.
        if (self.asset?.canPerformEditOperation(PHAssetEditOperation.Content) == true) {
            
            // Twirl이라는 이름을 가진 Action을 추가해준다. 이는 Action Sheet에서 버튼으로 보여진다.
            alert.addAction(UIAlertAction(title: "Twirl", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                
                // Twirl 효과는 몇 가지 옵션사항을 추가할 수 있다. applyFilter 메소드로 옵션을 보내기 위해 딕셔너리를 생성하여 적절한 옵션값을 추가한다.
                var optionValues:[String:AnyObject] = [String:AnyObject]()
                optionValues[kCIInputCenterKey] = CIVector(CGPoint: CGPointMake(200, 100))
                optionValues[kCIInputRadiusKey] = NSNumber(float: 500)
                
                // 필터 이름과 옵션값을 담은 딕셔너리를 인자로 applyFilter 메소드를 호출한다.
                self.applyFilter("CITwirlDistortion", optionValue: optionValues)
            }))
            
            // Mono라는 이름을 가진 Action을 추가해준다. 이는 Action Sheet에서 버튼으로 보여진다.
            alert.addAction(UIAlertAction(title: "Mono", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                
                // 필터 이름을 인자로 applyFilter 메소드를 호출한다.
                // Mono 효과는 옵션값을 넣지 않고 효출한다. applyFilter의 인자 중 optionValue는 기본값이 nil로 설정되어 있으므로 인자를 넘기지 않아도된다.
                self.applyFilter("CIPhotoEffectMono")
            }))
            
            // Revert라는 이름을 가진 Action을 추가해준다. 이는 Action Sheet에서 버튼으로 보여진다.
            alert.addAction(UIAlertAction(title: "Revert", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                
                // 사진 라이브러리에 원하는 변경사항을 요청한다.
                PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
                    
                    // Asset을 변경할 변경요청 생성
                    let request: PHAssetChangeRequest = PHAssetChangeRequest(forAsset: self.asset)
                    
                    // Asset을 최초 원본으로 되돌리도록 요청한다.
                    request.revertAssetContentToOriginal()
                    }, completionHandler: { (success: Bool, error: NSError!) -> Void in
                        
                        // 요청에 대한 결과를 확인해 볼 수 있다.
                        println("Change request result \nSuccess : \(success)\nError: \(error)")
                        // 결과에 따라 적절한 대응처리를 할 수 있다.
                })
            }))
        }
        
        // Asset이 속성을 수정할 수 있는지 체크한다.
        if (self.asset?.canPerformEditOperation(PHAssetEditOperation.Properties) == true) {
            
            // Action의 title로 사용될 String 상수
            // 현재 Asset이 좋아요 상태이면 Unfavorite을, 그렇지 않으면 Favorite이라고 넣어준다.
            let title = self.asset?.favorite == true ? "Unfavorite" : "Favorite"
            
            // title에 해당하는 이름을 가진 Action을 추가해준다. 이는 Action Sheet에서 버튼으로 보여진다.
            alert.addAction(UIAlertAction(title: title, style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                
                // 사진 라이브러리에 원하는 변경사항을 요청한다.
                PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
                    
                    // Asset을 변경할 변경요청 생성
                    let reqeust: PHAssetChangeRequest = PHAssetChangeRequest(forAsset: self.asset)
                    
                    // Asset이 좋아요 상태인지 아닌지 Bool flag를 세팅한다.
                    // 현재 Asset이 좋아요 상태이면 false를, 그렇지 않으면 true로 세팅한다.
                    reqeust.favorite = self.asset?.favorite == true ? false : true
                    
                    }, completionHandler: { (success: Bool, error: NSError!) -> Void in
                        
                        // 요청에 대한 결과를 확인해 볼 수 있다.
                        println("Change request result \nSuccess : \(success)\nError: \(error)")
                        // 결과에 따라 적절한 대응처리를 할 수 있다.
                })
            }))
        }
        
        // Asset이 삭제될 수 있는지 체크한다.
        if (self.asset?.canPerformEditOperation(PHAssetEditOperation.Delete) == true) {
            
            // DELETE라는 이름을 가진 Action을 추가해준다. 이는 Action Sheet에서 버튼으로 보여진다.
            alert.addAction(UIAlertAction(title: "DELETE", style: UIAlertActionStyle.Destructive, handler: { (action: UIAlertAction!) -> Void in
                
                // 사진 라이브러리에 원하는 변경사항을 요청한다.
                PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
                    
                    // Asset 삭제를 요청한다.
                    PHAssetChangeRequest.deleteAssets([self.asset!])
                    }, completionHandler: { (success: Bool, error: NSError!) -> Void in
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            
                            if success == true{  // 삭제가 성공이면
                                
                                // 네비게이션 컨트롤러를 통해 전 화면으로 돌아간다.
                                self.navigationController?.popViewControllerAnimated(true)
                            } else { // 삭제에 실패하면
                                
                                // 로그를 남긴다.
                                println("Delete request result \nSuccess : \(success)\nError: \(error)")
                            }
                        })
                })
            }))
        }
        
        // 화면에 Alert Controller를 보여준다.
        self.presentViewController(alert, animated: true, completion: nil)
    }

    // MARK: - PHPhotoLibraryChangeObserver
    func photoLibraryDidChange(changeInstance: PHChange!) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            // 변경된 자세한 사항을 가져온다.
            if let changedDetails: PHObjectChangeDetails = changeInstance.changeDetailsForObject(self.asset) {
                
                // 현재 Asset을 변경된 객체로 변경한다.
                self.asset = changedDetails.objectAfterChanges as? PHAsset
                
                // 변경사항이 있으면
                if changedDetails.assetContentChanged {
                    // 업데이트 해준다.
                    self.updateImage()
                }
            }
        })
    }
}
