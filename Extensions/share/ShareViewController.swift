//
//  ShareViewController.swift
//  share
//
//  Created by yagom on 2015. 3. 18..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import Social

class ShareViewController: SLComposeServiceViewController {
    
    // 공유할 때 적절한 컨텐츠를 작성하고 있는지 체크하여 허용여부를 결정합니다.
    override func isContentValid() -> Bool {
        
        // 예를 들어 메세지의 길이를 체크합니다.
        
        // 메세지 길이를 가져와서
        let messageLength = count(self.contentText)
        
        // 10글자가 넘어가면
        if messageLength > 10 {
            // 불허합니다. (화면에서 Post 버튼이 비활성화 됩니다)
            return false
        }
        
        // 그렇지 않다면 공유를 허용합니다.
        return true
    }
    /*
    // 이곳에 적절한 뷰를 생성하면 미리보기 화면을 직접 제작할 수 있습니다.
    override func loadPreviewView() -> UIView! {
    return nil
    }
    */
    
    /*
    // 취소 버튼을 선택하면 호출되는 함수입니다.
    override func didSelectCancel() {
    
    }
    */
    
    // 공유화면에서 Post 버튼을 눌렀을 때 호출되는 함수입니다.
    override func didSelectPost() {
        
        // 공유를 위해 선택된 아이템들
        let inputItems: [NSExtensionItem]? = self.extensionContext?.inputItems as? [NSExtensionItem]
        
        
        // 만약 아이템들이 nil이 아니라면
        if let items = inputItems {
            
            // 이곳에다 적절히 커스텀 공유 작업을 진행하는 코드를 작성합니다.
            /*
            ...
            */
            
            // 호스트 앱에게 작업이 완료되었음을 알려줍니다.
            self.extensionContext!.completeRequestReturningItems(items, completionHandler: nil)
        } else {
            // 아이템이 제대로 전달되어 오지 않았다면 취소합니다.
            self.didSelectCancel()
        }
    }
    
    // 제어 옵션 등을 선택할 수 있는 아이템을 반환하는 함수입니다.
    override func configurationItems() -> [AnyObject]! {
        
        // 임의의 아이템을 만들어 봅니다.
        var item = SLComposeSheetConfigurationItem()
        
        // 버튼의 제목을 정해주고
        item.title = "Configure something"
        
        // 터치하였을 때 수행할 동작을 클로저로 구현해 줍니다.
        // pushConfigurationViewController 함수 등을 참고하면 더 좋습니다.
        item.tapHandler = {
            
            let alert = UIAlertController(title: nil, message: "Configure", preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil)
            
            alert.addAction(okAction)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        // 아이템은 여러개를 배열의 형태로 전달합니다.
        return [item]
    }
}
