//
//  PhotoEditingViewController.swift
//  PhotoEditing
//
//  Created by yagom on 2015. 4. 5..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

class PhotoEditingViewController: UIViewController, PHContentEditingController {
    
    // 화면에 IBOutlet으로 연결될 이미지뷰
    @IBOutlet weak var imageView:UIImageView!
    
    // 편집될 데이터 입력
    var input: PHContentEditingInput?
    
    // MARK: - PHContentEditingController
    func canHandleAdjustmentData(adjustmentData: PHAdjustmentData?) -> Bool {
        // adjustmentData가 지금 익스텐션에서 동작될 수 있는지를 판단합니다.
        // 보통 formatIdentifier와 formatVersion 속성을 통해 판단합니다.
        // 지금 예제에서는 특별히 신경쓰지 않도록 하겠습니다.
        return false
    }
    
    // 입력받은 데이터를 편집하기 시작할 때 호출되는 함수입니다.
    func startContentEditingWithInput(contentEditingInput: PHContentEditingInput?, placeholderImage: UIImage) {
        // contentEditingInput은 편집세션이 종료되기 전에 사용하기 위해 보관해 둡니다.
        self.input = contentEditingInput
        
        // 현재 이미지를 이미지뷰의 이미지로 세팅해줍니다.
        self.imageView.image = placeholderImage
        
        // 세피아 톤 필터를 만들어줍니다.
        let filter = CIFilter(name: "CISepiaTone")
        
        // 입력받은 이미지를 CIImage로 변환해 줍니다.
        var inputImage = CIImage(CGImage: placeholderImage.CGImage)
        
        // 이미지의 회전을 세팅해주고
        inputImage = inputImage.imageByApplyingOrientation(Int32(self.imageView.image!.imageOrientation.rawValue))
        
        // 필터를 적용합니다.
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        
        // 결과물을 만들기 위한 컨텍스트
        let context = CIContext(options: nil)
        
        // 필터가 적용된 출력 이미지
        var outputImage = filter.outputImage
        
        // 컨텍스트를 통해 CGImage를 생성합니다.
        let cgImage = context.createCGImage(outputImage, fromRect: outputImage.extent())
        
        // 최종 UIImage 생성
        let finalImage = UIImage(CGImage: cgImage)
        
        // 최종 이미지를 화면의 이미지뷰에 세팅해줍니다.
        self.imageView.image = finalImage
    }
    
    // 편집화면에서 사용자가 완료를 선택했을 때 호출되는 함수입니다.
    func finishContentEditingWithCompletionHandler(completionHandler: ((PHContentEditingOutput!) -> Void)!) {
        // 백그라운드 큐에서 결과물을 생성해 제공합니다.
        dispatch_async(dispatch_get_global_queue(CLong(DISPATCH_QUEUE_PRIORITY_DEFAULT), 0)) {

            // 미리 보관해둔 input을 통해 output을 생성합니다.
            let output = PHContentEditingOutput(contentEditingInput: self.input)
            
            // 세피아톤 키를 가지고 아카이브 데이터를 생성합니다.
            let arcivedData = NSKeyedArchiver.archivedDataWithRootObject("CISepiaTone")
            
            // 보정정보를 생성합니다. formatIdentifier는 주로 번들ID를 사용합니다.
            let adjustmentData = PHAdjustmentData(formatIdentifier: "net.yagom.Extensions.PhotoEditing", formatVersion: "1.0", data: arcivedData)
            
            // 출력에 보정정보를 세팅해주고
            output.adjustmentData = adjustmentData
            
            // JPEG 데이터를 생성해줍니다.
            let renderedJPEGData = UIImageJPEGRepresentation(self.imageView.image, 1.0)
            
            // 지정된 URL에 JPEG 데이터를 써줍니다.
            renderedJPEGData.writeToURL(output.renderedContentURL, atomically: true)
            
            // 결과물 제출을 위해 completionHandler를 호출해줍니다.
            completionHandler?(output)
        }
    }
    
    // 사용자가 취소 버튼을 선택했을 때 취소 확인을 보여줄 것인지 아닌지 결정합니다.
    var shouldShowCancelConfirmation: Bool {
        return true
    }
    
    // 사용자가 취소 버튼을 통해 취소했을 때 호출되는 함수입니다.
    func cancelContentEditing() {
    }
}
