//
//  ActionViewController.swift
//  Action
//
//  Created by yagom on 2015. 4. 2..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation

class ActionViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        // for문을 실행할 때 텍스트를 찾았는지 확인해줄 flag
        var textFound = false
        
        // 익스텐션을 실행할 때 함께 들어온 아이템들을 체크하며 순회할 for문
        for item: AnyObject in self.extensionContext!.inputItems {
            
            // 들어온 아이템 중 하나
            let inputItem = item as! NSExtensionItem
            
            // 들어온 아이템의 첨부데이터들을 순회하는 for문
            for provider: AnyObject in inputItem.attachments! {
                
                // item 제공자
                let itemProvider = provider as! NSItemProvider
                
                // item 제공자가 텍스트를 가지고 있다면
                if itemProvider.hasItemConformingToTypeIdentifier(kUTTypePlainText as String) {
                    
                    // 화면에 있는 텍스트뷰 - 튜플 안에서는 self 키워드를 사용하여 접근하면 메모리관리가 어려워질 수 있으므로 weak 변수를 생성해준다
                    weak var weakTextView = self.textView
                    
                    // item 제공자에게서 텍스트를 가져온다
                    itemProvider.loadItemForTypeIdentifier(kUTTypePlainText as String, options: nil, completionHandler: { (text, error) in
                        
                        // 텍스트가 nil이 아니라면
                        if text != nil {
                            
                            // Main Operation Queue에 작업을 추가해준다
                            NSOperationQueue.mainQueue().addOperationWithBlock {
                                
                                // 텍스트뷰가 존재한다면
                                if let textView = weakTextView {
                                    
                                    // 가져온 텍스트를 세팅해주고
                                    textView.text = text as? String
                                    
                                    // 텍스트를 읽어주기위한 AVSpeechSynthesizer생성
                                    let speecher = AVSpeechSynthesizer()
                                    
                                    // 텍스트를 읽어주기 위한 Utterance(발언자) 생성
                                    let utterance = AVSpeechUtterance(string: text as! String)
                                    
                                    // 최대한 느리게 읽어주세요. 가장 듣기 좋은 rate는 0.1 정도입니다.
                                    utterance.rate = AVSpeechUtteranceMinimumSpeechRate
                                    
                                    // 한글을 읽기 위한 언어설정
                                    utterance.voice = AVSpeechSynthesisVoice(language: "ko-KR")
                                    
                                    // 읽어주세요
                                    speecher.speakUtterance(utterance)
                                }
                            }
                        }
                    })
                    
                    // 텍스트를 찾았다는 flag를 설정해줍니다.
                    textFound = true
                    break
                }
            }
            
            if (textFound) {
                // 우리는 텍스트만 취급해 줄 것이므로 텍스트를 발견했다면 더이상 실행할 필요가 없습니다.
                break
            }
        }
    }

    @IBAction func done() {
        // 액션을 실행한 후 호스트 어플리케이션에 응답을 돌려보냅니다. 만약 item을 수정했다면 수정한 item을 넣어주면 됩니다.
        self.extensionContext!.completeRequestReturningItems(self.extensionContext!.inputItems, completionHandler: nil)
    }

}
