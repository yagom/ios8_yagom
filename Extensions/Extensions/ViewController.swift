//
//  ViewController.swift
//  Extensions
//
//  Created by yagom on 2015. 3. 18..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // 스토리보드의 텍스트뷰와 연결된 IBOutlet
    @IBOutlet weak var textView: UITextView!

    // 스토리보드의 액션 버튼과 연결된 IBAction
    @IBAction func clickActionButton(sender: UIBarButtonItem) {
        
        // 텍스트뷰의 텍스트를 아이템으로 하는 액티비티 컨트롤러 생성
        let activityViewController = UIActivityViewController(activityItems: [self.textView.text], applicationActivities: nil)
        
        // 액티비티 컨트롤러를 화면에 보여준다
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
}

