//
//  KeyboardViewController.swift
//  Keyboard
//
//  Created by yagom on 2015. 4. 6..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit

// 다음 키보드로 이동하기 위한 버튼을 xib 파일에서 가져온 후 구분해주기 위해 Tag를 사용합니다.
let NextButtonTag: Int = 100

class KeyboardViewController: UIInputViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // xib 파일을 로드합니다. 과거 nib라는 확장자를 가졌던 잔재 때문에 아직도 nib라는 이름으로 API가 구성되어 있습니다.
        let nib = UINib(nibName: "KeyboardView", bundle: nil)
        
        // xib 파일에 들어있는 객체들을 꺼내옵니다.
        let objects = nib.instantiateWithOwner(self, options: nil)
        
        // 그 중 맨 첫 객체를 현재 뷰컨트롤러의 view로 세팅해줍니다.
        // 맨 첫 객체를 꺼내와도 되는 이유는 xib에 하나의 뷰만 생성해 주었기 때문입니다.
        self.view = objects.first as? UIView;
        
        // 뷰의 자식뷰들을 순회하며 체크합니다.
        for subview in self.view.subviews {
            
            // 자식뷰가 만약 UIButton 클래스의 인스턴스라면
            if subview.isKindOfClass(UIButton) {
                
                // 태그를 확인합니다.
                // 만약 다음키보드로 가기 버튼이라면
                if subview.tag == NextButtonTag {
                    
                    // 다음 키보드를 호출하는 함수를 타겟으로 추가해줍니다.
                    subview.addTarget(self, action: "advanceToNextInputMode", forControlEvents: .TouchUpInside)
                } else {
                    
                    // 그렇지 않다면 키보드 버튼을 누를때 호출할 clickButton 함수를 타겟으로 추가해줍니다.
                    subview.addTarget(self, action: "clickButton:", forControlEvents: .TouchUpInside)
                }
            }
        }
    }
    
    // 키보드 버튼을 누를때 호출할 함수입니다.
    func clickButton(sender: UIButton) {
        
        // 버튼의 타이틀 텍스트를 문서 프록시에 삽입해 줍니다.
        if let string: String = sender.titleForState(UIControlState.Normal) {
            // string 문자를 입력합니다.
            // 만약 백스페이스(한 글자 지우기)를 구현하고 싶다면 
            // (self.textDocumentProxy as UIKeyInput).deleteBackward() 라고 호출하면 됩니다.
            (self.textDocumentProxy as! UIKeyInput).insertText("\(string)")
        }
    }
}
