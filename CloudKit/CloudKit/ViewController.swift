//
//  ViewController.swift
//  CloudKit
//
//  Created by yagom on 2015. 4. 10..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import CloudKit

// 공통적으로 쓰일 상수들
private let MyRecordType = "Profile"
private let MyRecordID = "myRecord"

private let KeyName = "name"
private let KeyAge = "age"
private let KeyNickname = "nick"
private let KeyProfileImage = "img"

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // IBOutlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var nicknameField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    
    // 클라우드 킷 컨테이너 (클라우드 킷 대시보드에서 구분될 컨테이너)
    private lazy var container: CKContainer = CKContainer.defaultContainer()

    // 레코드 ID
    private let myRecordID: CKRecordID = CKRecordID(recordName: MyRecordID)
    
    // 컨테이너에 속한 레코드
    private var myRecord: CKRecord?
    
    // 우리는 컨테이너의 PUBLIC DATA에 읽고 씁니다. 
    // 다른 별도의 옵션을 주면 PRIVATE DATA의 ZONE에도 접근할 수 있습니다.
    private  var publicDatabase: CKDatabase {
        get {
            return self.container.publicCloudDatabase
        }
    }

    // 클라우드 킷 사용가능여부 flag
    private var isCloudAvailable: Bool = false
    
    // 이미지를 Picker에서 선택하여 업로드 하기 전에 임시로 이미지 파일 URL을 저장해 둡니다.
    private var localImageURL: NSURL?
    
    // 저장 버튼을 선택 했을 때
    @IBAction func clickSaveButton(sender: AnyObject) {
        self.saveProfileData()
    }
    
    // 사진 선택 버튼을 선택 했을 때
    @IBAction func clickSelectPhotoButton(sender: AnyObject) {
        
        // 이미지 피커를 화면에 보여줍니다.
        let picker = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker.delegate = self
        
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 제일 먼저 클라우드킷 사용 가능 여부를 체크합니다.
        self.checkCloudAvailableWithCompletionHandler { () -> Void in
            
            // 사용 가능 여부를 체크한 후 기존 레코드를 가져옵니다
            self.fetchProfileData()
        }
    }
    
    //MARK:- Check iCloud

    private func checkCloudAvailableWithCompletionHandler(completionHandler: () -> Void) {
        
        // 계정 사용 가능여부를 체크합니다.
        self.container.accountStatusWithCompletionHandler { (accountStatus: CKAccountStatus, error: NSError!) -> Void in
            
            // 사용 가능하다면
            if (accountStatus == CKAccountStatus.Available) {
                
                // flag를 사용가능으로 설정
                self.isCloudAvailable = true
                
            }
            else {
                // 사용가능하지 않다면 flag를 false로 설정해주고 사용자에게 사용 불가능함을 알립니다.
                self.isCloudAvailable = false
                let alert = UIAlertController(title: "알림", message: "iCloud에 로그인이 되어있지 않거나 접근이 허용되어있지 않습니다. 설정을 확인 후 다시 시도해주세요.\n\(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "확인", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            completionHandler()
        }
    }
    
    // 새로운 레코드 아이템을 만듭니다.
    private func makeNewRecordItem() -> CKRecord {
        return CKRecord(recordType: MyRecordType, recordID: self.myRecordID)
    }
    
    //MARK:- Fetch and Save
    
    private func fetchProfileData() {

        // 클라우드킷이 사용가능하지 않은 상태라면 더이상 실행하지 않습니다.
        if self.isCloudAvailable == false {
            println("Can not use iCloud")
            return
        }

        // 레코드ID를 사용하여 레코드를 가져옵니다.
        self.publicDatabase.fetchRecordWithID(self.myRecordID, completionHandler: { (record: CKRecord!, error: NSError!) -> Void in
            
            // 기존 레코드가 존재한다면
            if let record = record {
                self.myRecord = record
                
                // 기존 레코드 자료를 화면에 적절히 보여줍니다.
                if let name = self.myRecord?.objectForKey(KeyName) as? String {
                    self.nameField.text = name
                }
                if let nick = self.myRecord?.objectForKey(KeyNickname) as? String {
                    self.nicknameField.text = nick
                }
                if let age = self.myRecord?.objectForKey(KeyAge).integerValue {
                    self.ageField.text = "\(age)"
                }
                if let asset = self.myRecord?.objectForKey(KeyProfileImage) as? CKAsset{
                    
                    if let data =  NSData(contentsOfURL: asset.fileURL) {
                        self.imageView.image = UIImage(data: data)
                    }
                }
            } else {
                // 존재하지 않는다면
                println(error)
                
                // 새로운 레코드를 만듭니다.
                self.myRecord = self.makeNewRecordItem()
            }
        })
    }
    
    private func saveProfileData() {
        
        // 클라우드킷이 사용가능하지 않은 상태라면 더이상 실행하지 않습니다.
        if self.isCloudAvailable == false {
            println("Can not use iCloud")
            return
        }
        
        // 기존 레코드가 없다면 새로 만들어줍니다.
        if self.myRecord == nil {
            self.myRecord = self.makeNewRecordItem()
        }
        
        // 각각의 키에 맞게 값을 설정해줍니다.
        self.myRecord?.setObject(self.nameField.text, forKey: KeyName)
        self.myRecord?.setObject(self.ageField.text.toInt(), forKey: KeyAge)
        self.myRecord?.setObject(self.nicknameField.text, forKey: KeyNickname)
        
        if let url = self.localImageURL {
            
            let asset = CKAsset(fileURL: self.localImageURL)
            
            // 이미지 파일은 큰 바이너리 파일이기 때문에 Asset을 이용합니다.
            self.myRecord?.setObject(asset, forKey: KeyProfileImage)
        }
        
        // 레코드를 저장합니다.
        self.publicDatabase.saveRecord(self.myRecord, completionHandler: { (myRecord: CKRecord!, error: NSError!) -> Void in
            
            if let err = error {
                println(err)
            } else {
                
                // 저장이 완료되면 완료되었음을 알립니다.
                println("save record successfully")
                let alert = UIAlertController(title: "알림", message: "저장되었습니다.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "확인", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)

                // 임시로 저장해 두었던 이미지 파일이 있다면 삭제합니다.
                if let url = self.localImageURL {
                    
                    if NSFileManager.defaultManager().removeItemAtURL(url, error: nil) {
                        self.localImageURL = nil
                    }
                }
            }
        })
    }
    
    //MARK:- Image Picker Controller Delegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        // 임시로 저장할 이미지 경로를 생성합니다
        let imageURL = info[UIImagePickerControllerReferenceURL] as! NSURL
        let imageName = imageURL.path!.lastPathComponent
        let tempDirectory = NSTemporaryDirectory()
        let localPath = tempDirectory.stringByAppendingPathComponent(imageName)
        
        // 이미지를 가져와서
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let data = UIImageJPEGRepresentation(image, 1.0)
        
        // 임시 경로에 써줍니다.
        if data.writeToFile(localPath, atomically: true) {
            
            // 임시 파일 쓰기에 성공하면 프로퍼티에 URL을 저장해주고
            self.localImageURL = NSURL(fileURLWithPath: localPath)
            // 화면에 이미지를 보여줍니다
            self.imageView.image = image
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
}

