//
//  PreviewView.swift
//  ManualCamControl
//
//  Created by yagom on 2015. 4. 8..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import AVFoundation

class PreviewView: UIView {
    
    // 캡쳐 세션 프로퍼티
    var session: AVCaptureSession? {
        
        // session 프로퍼티의 getter와 setter를 구현해 줍니다.
        get {
            return (self.layer as! AVCaptureVideoPreviewLayer).session
        }
        
        set(newSession) {
            (self.layer as! AVCaptureVideoPreviewLayer).session = newSession
        }
    }

    // 기본 반환은 [CALayer class]이지만 뷰의 레이어를 다른 레이어로 대체하고자 할 때 오버라이드 해줍니다.
    override class func layerClass() -> AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
}
