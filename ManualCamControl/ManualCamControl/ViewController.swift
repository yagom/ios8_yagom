//
//  ViewController.swift
//  ManualCamControl
//
//  Created by yagom on 2015. 4. 8..
//  Copyright (c) 2015년 yagom. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary

class ViewController: UIViewController, AVCaptureFileOutputRecordingDelegate, UIGestureRecognizerDelegate {
    
    // 화면 구성요소와 연결될 IBOutlet들
    @IBOutlet private weak var previewView: PreviewView!
    @IBOutlet private weak var recordButton: UIButton!
    @IBOutlet private weak var cameraButton: UIButton!
    @IBOutlet private weak var stillButton: UIButton!
    
    private var focusModes: [NSNumber]
    @IBOutlet private weak var manualHUDFocusView: UIView!
    @IBOutlet private weak var focusModeControl: UISegmentedControl!
    @IBOutlet private weak var lensPositionSlider: UISlider!
    @IBOutlet private weak var lensPositionNameLabel: UILabel!
    @IBOutlet private weak var lensPositionValueLabel: UILabel!
    
    private var exposureModes: [NSNumber]
    @IBOutlet private weak var manualHUDExposureView: UIView!
    @IBOutlet private weak var exposureModeControl: UISegmentedControl!
    @IBOutlet private weak var exposureDurationSlider: UISlider!
    @IBOutlet private weak var exposureDurationNameLabel: UILabel!
    @IBOutlet private weak var exposureDurationValueLabel: UILabel!
    @IBOutlet private weak var ISOSlider: UISlider!
    @IBOutlet private weak var ISONameLabel: UILabel!
    @IBOutlet private weak var ISOValueLabel: UILabel!
    @IBOutlet private weak var exposureTargetBiasSlider: UISlider!
    @IBOutlet private weak var exposureTargetBiasNameLabel: UILabel!
    @IBOutlet private weak var exposureTargetBiasValueLabel: UILabel!
    @IBOutlet private weak var exposureTargetOffsetSlider: UISlider!
    @IBOutlet private weak var exposureTargetOffsetNameLabel: UILabel!
    @IBOutlet private weak var exposureTargetOffsetValueLabel: UILabel!
    
    private var whiteBalanceModes: [NSNumber]
    @IBOutlet private weak var manualHUDWhiteBalanceView: UIView!
    @IBOutlet private weak var whiteBalanceModeControl: UISegmentedControl!
    @IBOutlet private weak var temperatureSlider: UISlider!
    @IBOutlet private weak var temperatureNameLabel: UILabel!
    @IBOutlet private weak var temperatureValueLabel: UILabel!
    @IBOutlet private weak var tintSlider: UISlider!
    @IBOutlet private weak var tintNameLabel: UILabel!
    @IBOutlet private weak var tintValueLabel: UILabel!

    // sessionQueue를 따로 생성하는 이유는 세션이 실행 될 때 메인큐에서 실행되면 로딩되는 동안 화면멈춤 등의 블락 현상이 일어날 수 있기 때문에 따로 큐를 생성해 줍니다.
    private lazy var sessionQueue: dispatch_queue_t = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL)
    private lazy var session: AVCaptureSession = AVCaptureSession()
    
    // dynamic 키워드를 붙여주어 Key-Value-Observing을 사용할 수 있습니다.
    private dynamic var videoDeviceInput: AVCaptureDeviceInput?
    private lazy var videoDevice: AVCaptureDevice = ViewController.device(AVMediaTypeVideo, preferringPosition: AVCaptureDevicePosition.Back)
    private dynamic var movieFileOutput: AVCaptureMovieFileOutput = AVCaptureMovieFileOutput()
    private dynamic var stillImageOutput: AVCaptureStillImageOutput = AVCaptureStillImageOutput()
    
    private var backgroundRecordingID: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    private var deviceAuthorized: Bool
    // Objective-C와 달리 getter이름을 따로 정해줄 수 없기 때문에 readonly 계산 프로퍼티를 하나 생성해 주었습니다.
    private var isDeviceAuthorized: Bool {
        get {
            return self.deviceAuthorized
        }
    }
    
    private var isSessionRunningAndDeviceAuthorized: Bool {
        get {
            return self.session.running && self.isDeviceAuthorized
        }
    }
    
    private var lockInterfaceRotation: Bool
    private var runtimeErrorHandlingObserver: AnyObject?
    
    private let CONTROL_NORMAL_COLOR: UIColor = UIColor.yellowColor()
    // 예쁜 파랑색이래요
    private let CONTROL_HIGHLIGHT_COLOR: UIColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
    // 더 높은 숫자일수록 슬라이더 들이 더 민감하게 반응할 것입니다
    private let EXPOSURE_DURATION_POWER: Double = 5
    // 유용한 범위를 위해 노출 시간을 제한합니다
    private let EXPOSURE_MINIMUM_DURATION: Float64 = 1.0/1000.0
    
    private class func keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized() -> NSSet {
        return NSSet(objects: ("session.running" as NSString), ("deviceAuthorized" as NSString))
    }
    
    // Key-Value-Observing에 사용될 context들
    private var CapturingStillImageContext = 0
    private var RecordingContext = 0
    private var SessionRunningAndDeviceAuthorizedContext = 0
    
    private var FocusModeContext = 0
    private var ExposureModeContext = 0
    private var WhiteBalanceModeContext = 0
    private var LensPositionContext = 0
    private var ExposureDurationContext = 0
    private var ISOContext = 0
    private var ExposureTargetOffsetContext = 0
    private var DeviceWhiteBalanceGainsContext = 0
    
    //MARK:- Life Cycle
    
    // 기본적으로 init 할 때 초기화해 줄 인스턴스 변수들을 초기화 해줍니다.
    required init(coder aDecoder: NSCoder) {
        self.focusModes = [AVCaptureFocusMode.ContinuousAutoFocus.rawValue, AVCaptureFocusMode.Locked.rawValue]
        self.exposureModes = [AVCaptureExposureMode.ContinuousAutoExposure.rawValue, AVCaptureExposureMode.Locked.rawValue, AVCaptureExposureMode.Custom.rawValue]
        self.whiteBalanceModes = [AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance.rawValue, AVCaptureWhiteBalanceMode.Locked.rawValue]
        self.deviceAuthorized = false
        self.lockInterfaceRotation = false
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        self.recordButton.layer.cornerRadius = 4
        self.stillButton.layer.cornerRadius = 4
        self.cameraButton.layer.cornerRadius = 4
        
        self.recordButton.clipsToBounds = true
        self.stillButton.clipsToBounds = true
        self.cameraButton.clipsToBounds = true

        self.previewView.session = self.session
        
        self.checkDeviceAuthorizationStatus()
        
        dispatch_async(self.sessionQueue, { () -> Void in
            self.backgroundRecordingID = UIBackgroundTaskInvalid
            
            var error: NSError? = nil
            
            self.videoDevice = ViewController.device(AVMediaTypeVideo, preferringPosition: AVCaptureDevicePosition.Back)
            self.videoDeviceInput = AVCaptureDeviceInput(device: self.videoDevice, error: &error)
            
            if let err = error {
                println(err)
            }
            
            self.session.beginConfiguration()
            
            if self.session.canAddInput(self.videoDeviceInput) {
                self.session.addInput(self.videoDeviceInput)
                self.videoDevice = self.videoDeviceInput!.device!
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    // 이 부분은 메인큐에서 진행하는 이유는 UI에 관련된 작업이기 때문입니다.
                    // UI관련 작업은 메인큐에서 진행해야 합니다.
                    (self.previewView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation = AVCaptureVideoOrientation(rawValue: self.interfaceOrientation.rawValue)!
                })
                error = nil
                var audioDevice: AVCaptureDevice = AVCaptureDevice.devicesWithMediaType(AVMediaTypeAudio).first as! AVCaptureDevice
                var audioDeviceInput: AVCaptureDeviceInput = AVCaptureDeviceInput.deviceInputWithDevice(audioDevice, error: &error) as! AVCaptureDeviceInput
                
                if let err = error {
                    println(err)
                }
                
                if self.session.canAddInput(audioDeviceInput) {
                    self.session.addInput(audioDeviceInput)
                }
                
                if self.session.canAddOutput(self.movieFileOutput) {
                    self.session.addOutput(self.movieFileOutput)
                    
                    let connection: AVCaptureConnection = self.movieFileOutput.connectionWithMediaType(AVMediaTypeVideo)
                    
                    if connection.supportsVideoStabilization {
                        connection.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.Auto
                    }
                }
                
                if self.session.canAddOutput(self.stillImageOutput) {
                    self.stillImageOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                    self.session.addOutput(self.stillImageOutput)
                }
                
                self.session.commitConfiguration()
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.configureManualHUD()
                })
                
                self.manualHUDFocusView.hidden = true
                self.manualHUDExposureView.hidden = true
                self.manualHUDWhiteBalanceView.hidden = true
            }
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        dispatch_async(self.sessionQueue, { () -> Void in
            self.addObservers()
            self.session.startRunning()
        })
    }
    
    override func viewDidDisappear(animated: Bool) {
        dispatch_async(self.sessionQueue, { () -> Void in
            self.session.stopRunning()
            
            self.removeObservers()
        })
        
        super.viewDidDisappear(animated)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func shouldAutorotate() -> Bool {
        return !self.lockInterfaceRotation
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.All.rawValue)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        (self.previewView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation = AVCaptureVideoOrientation(rawValue: toInterfaceOrientation.rawValue)!
    }
    
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        self.positionManualHUD()
    }
    
    //MARK:- Actions
    @IBAction private func toggleMovieRecording(sender: AnyObject) {
        self.recordButton.enabled = false

        dispatch_async(self.sessionQueue, { () -> Void in
            if self.movieFileOutput.recording == false {
                self.lockInterfaceRotation = true
                
                if UIDevice.currentDevice().multitaskingSupported {
                    
                    // 백그라운드 실행 시간을 요청하지 않으면 captureOutput:didFinishRecordingToOutputFileAtURL: 콜백 함수를 앱이 foreground 모드로 돌아오기 전까지는 받을 수 없기 때문에 백그라운드 테스크를 설정해줍니다. 또한 백그라운드 상태가 되었을 때에도 에셋 라이브러리에 파일을 쓰는 시간을 보장해줍니다. -endBackgroundTask는 -recorder:recordingDidFinishToOutputFileURL:error:에서 파일쓰기가 완료되면 호출됩니다.
                    self.backgroundRecordingID = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({ () -> Void in
                    })
                }
                
                // 레코드 하기 전에 화면 방향을 업데이트 해줍니다.
                self.movieFileOutput.connectionWithMediaType(AVMediaTypeVideo).videoOrientation = (self.previewView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation
                
                // 비디오 레코딩을 위해 플래시 종료
                ViewController.setFlashMode(AVCaptureFlashMode.Off, forDevice: self.videoDevice)
                
                // 임시파일에 레코드를 시작합니다.
                let outputFilePath: String = NSTemporaryDirectory().stringByAppendingPathComponent("movie".stringByAppendingPathExtension("mov")!)
                self.movieFileOutput.startRecordingToOutputFileURL(NSURL(fileURLWithPath: outputFilePath), recordingDelegate: self)
            } else {
                self.movieFileOutput.stopRecording()
            }
        })
    }
    
    @IBAction private func changeCamera(sender: AnyObject) {

        self.cameraButton.enabled = false
        self.recordButton.enabled = false
        self.stillButton.enabled = false
        
        dispatch_async(self.sessionQueue, { () -> Void in
            let currentVideoDevice = self.videoDevice
            var preferredPosition = AVCaptureDevicePosition.Unspecified
            let currentPosition = currentVideoDevice.position
            
            switch currentPosition {
            case AVCaptureDevicePosition.Unspecified:
                preferredPosition = AVCaptureDevicePosition.Back
                
            case AVCaptureDevicePosition.Back:
                preferredPosition = AVCaptureDevicePosition.Front
                
            case AVCaptureDevicePosition.Front:
                preferredPosition = AVCaptureDevicePosition.Back
            }
            
            let newVideoDevice = ViewController.device(AVMediaTypeVideo, preferringPosition: preferredPosition)
            let newVideoDeviceInput = AVCaptureDeviceInput(device: newVideoDevice, error: nil)
            
            self.session.beginConfiguration()
            
            self.session.removeInput(self.videoDeviceInput)
            
            if self.session.canAddInput(newVideoDeviceInput) {
                NSNotificationCenter.defaultCenter().removeObserver(self, name: AVCaptureDeviceSubjectAreaDidChangeNotification, object: currentVideoDevice)
                
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "subjectAreaDidChange:", name: AVCaptureDeviceSubjectAreaDidChangeNotification, object: newVideoDevice)
                
                self.session.addInput(newVideoDeviceInput)
                self.videoDeviceInput = newVideoDeviceInput
                self.videoDevice = newVideoDevice
            } else {
                self.session.addInput(self.videoDeviceInput)
            }
            
            self.session.commitConfiguration()
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.cameraButton.enabled = true
                self.recordButton.enabled = true
                self.stillButton.enabled = true
                
                self.configureManualHUD()
            })
        })
    }
    
    @IBAction private func snapStillImage(sender: AnyObject) {

        dispatch_async(self.sessionQueue, { () -> Void in
            // 캡쳐하기 전에 화면 방향을 업데이트 해줍니다.
            self.stillImageOutput.connectionWithMediaType(AVMediaTypeVideo).videoOrientation = (self.previewView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation
            
            // 캡쳐를 위해 플래시를 자동으로 설정합니다.
            if self.videoDevice.exposureMode == AVCaptureExposureMode.Custom {
                ViewController.setFlashMode(AVCaptureFlashMode.Off, forDevice: self.videoDevice)
            } else {
                ViewController.setFlashMode(AVCaptureFlashMode.Auto, forDevice: self.videoDevice)
            }
            
            // 이미지를 캡쳐합니다.
            self.stillImageOutput.captureStillImageAsynchronouslyFromConnection(self.stillImageOutput.connectionWithMediaType(AVMediaTypeVideo), completionHandler: { (imageDataSampleBuffer: CMSampleBuffer!, error: NSError!) -> Void in
                
                if let buffer = imageDataSampleBuffer {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                    
                    if let data = imageData {
                        if let image = UIImage(data: data) {
                            
                            ALAssetsLibrary().writeImageToSavedPhotosAlbum(image.CGImage, orientation:ALAssetOrientation(rawValue:image.imageOrientation.rawValue)!, completionBlock: nil)
                        }
                    }
                }
            })
        })
    }
    
    @IBAction private func focusAndExposeTap(gestureRecognizer: UIGestureRecognizer) {

        if self.videoDevice.focusMode != AVCaptureFocusMode.Locked && self.videoDevice.exposureMode != AVCaptureExposureMode.Custom {
            let devicePoint = (self.previewView.layer as! AVCaptureVideoPreviewLayer).captureDevicePointOfInterestForPoint(gestureRecognizer.locationInView(gestureRecognizer.view))
            
            self.focusAndExposeWith(AVCaptureFocusMode.ContinuousAutoFocus, exposureMode: AVCaptureExposureMode.ContinuousAutoExposure, atDevicePoint: devicePoint, monitorSubjectAreaChange: true)
        }
    }
    
    @IBAction private func changeManualHUD(sender: UISegmentedControl) {
        
        self.positionManualHUD()
        
        self.manualHUDFocusView.hidden = sender.selectedSegmentIndex == 1 ? false : true
        self.manualHUDExposureView.hidden = sender.selectedSegmentIndex == 2 ? false : true
        self.manualHUDWhiteBalanceView.hidden = sender.selectedSegmentIndex == 3 ? false : true
    }
    
    @IBAction private func changeFocusMode(sender: UISegmentedControl) {
        
        let mode: AVCaptureFocusMode = AVCaptureFocusMode(rawValue: self.focusModes[sender.selectedSegmentIndex].integerValue)!
        var error: NSError? = nil
        
        if self.videoDevice.lockForConfiguration(&error) {
            if self.videoDevice.isFocusModeSupported(mode) {
                self.videoDevice.focusMode = mode
            } else {
                println("Focus mode \(self.stringFromFocusMode(mode)) is not supported. Focus mode is \(self.stringFromFocusMode(self.videoDevice.focusMode))")
                self.focusModeControl.selectedSegmentIndex = find(self.focusModes, self.videoDevice.focusMode.rawValue)!
            }
        } else if let err = error {
            println(err)
        }
    }
    
    @IBAction private func changeExposureMode(sender: UISegmentedControl) {
        
        let mode: AVCaptureExposureMode = AVCaptureExposureMode(rawValue: self.exposureModes[sender.selectedSegmentIndex].integerValue)!
        var error: NSError? = nil
        
        if self.videoDevice.lockForConfiguration(&error) {
            if self.videoDevice.isExposureModeSupported(mode) {
                self.videoDevice.exposureMode = mode
            } else {
                println("Exposure mode \(self.stringFromExposureMode(mode)) is not supported. Exposure mode is \(self.stringFromExposureMode(self.videoDevice.exposureMode))")
            }
        } else if let err = error {
            println(err)
        }
    }
    
    @IBAction private func changeWhiteBalanceMode(sender: UISegmentedControl) {
        
        let mode: AVCaptureWhiteBalanceMode = AVCaptureWhiteBalanceMode(rawValue: self.whiteBalanceModes[sender.selectedSegmentIndex].integerValue)!
        var error: NSError? = nil
        
        if self.videoDevice.lockForConfiguration(&error) {
            if self.videoDevice.isWhiteBalanceModeSupported(mode) {
                self.videoDevice.whiteBalanceMode = mode
            } else {
                println("White balance mode \(self.stringFromWhiteBalanceMode(mode)) is not supported. White balance mode is \(self.stringFromWhiteBalanceMode(self.videoDevice.whiteBalanceMode))")
            }
        } else if let err = error {
            println(err)
        }
    }
    
    @IBAction private func changeLensPosition(sender: UISlider) {
        
        var error: NSError? = nil
        
        if self.videoDevice.lockForConfiguration(&error) {
            self.videoDevice.setFocusModeLockedWithLensPosition(sender.value, completionHandler: nil)
        } else if let err = error {
            println(err)
        }
    }
    
    @IBAction private func changeExposureDuration(sender: UISlider) {
        
        var error: NSError? = nil
        
        let p: Double = pow(Double(sender.value), EXPOSURE_DURATION_POWER)// 슬라이더의 최저 범위를 확장하기 위해 power 함수를 사용합니다
        let minDurationSeconds: Double = max(CMTimeGetSeconds(self.videoDevice.activeFormat.minExposureDuration), EXPOSURE_MINIMUM_DURATION)
        let maxDurationSeconds: Double = CMTimeGetSeconds(self.videoDevice.activeFormat.maxExposureDuration)
        let newDurationSeconds: Double = p * (maxDurationSeconds - minDurationSeconds) + minDurationSeconds// 슬라이더의 0~1 사이의 값으로 실제 노출시간을 구합니다
        
        if self.videoDevice.exposureMode == AVCaptureExposureMode.Custom {
            if newDurationSeconds < 1 {
                let digits = max(0, 2 + floor(log10(newDurationSeconds)))
                self.exposureDurationValueLabel.text = NSString(format: "1/%.*f", digits, 1 / newDurationSeconds) as String
            } else {
                self.exposureDurationValueLabel.text = NSString(format: "%.2f", newDurationSeconds) as String
            }
        }
        
        if self.videoDevice.lockForConfiguration(&error) {
            self.videoDevice.setExposureModeCustomWithDuration(CMTimeMakeWithSeconds(newDurationSeconds, 1000 * 1000), ISO: AVCaptureISOCurrent, completionHandler: nil)
        } else if let err = error {
            println(err)
        }
    }
    
    @IBAction private func changeISO(sender: UISlider) {
        
        var error: NSError? = nil
        
        if self.videoDevice.lockForConfiguration(&error) {
            self.videoDevice.setExposureModeCustomWithDuration(AVCaptureExposureDurationCurrent, ISO: sender.value, completionHandler: nil)
        } else if let err = error {
            println(err)
        }
    }
    
    @IBAction private func changeExposureTargetBias(sender: UISlider) {
        
        var error: NSError? = nil
        
        if self.videoDevice.lockForConfiguration(&error) {
            self.videoDevice.setExposureTargetBias(sender.value, completionHandler: nil)
            self.exposureTargetBiasValueLabel.text = NSString(format: "%.1f", sender.value) as String
        } else if let err = error {
            println(err)
        }
    }
    
    @IBAction private func changeTemperature(sender: AnyObject) {
        
        let temperatureAndTint: AVCaptureWhiteBalanceTemperatureAndTintValues = AVCaptureWhiteBalanceTemperatureAndTintValues(temperature: self.temperatureSlider.value, tint: self.tintSlider.value)
        
        self.setWhiteBalanceGains(self.videoDevice.deviceWhiteBalanceGainsForTemperatureAndTintValues(temperatureAndTint))
    }
    
    @IBAction private func changeTint(sender: AnyObject) {
        
        let temperatureAndTint: AVCaptureWhiteBalanceTemperatureAndTintValues = AVCaptureWhiteBalanceTemperatureAndTintValues(temperature: self.temperatureSlider.value, tint: self.tintSlider.value)
        
        self.setWhiteBalanceGains(self.videoDevice.deviceWhiteBalanceGainsForTemperatureAndTintValues(temperatureAndTint))
    }
    
    @IBAction private func lockWithGrayWorld(sender: AnyObject) {
        
        self.setWhiteBalanceGains(self.videoDevice.grayWorldDeviceWhiteBalanceGains)
    }
    
    @IBAction private func sliderTouchBegan(sender: UISlider) {
        
        self.setSlider(sender, highlightColor: CONTROL_HIGHLIGHT_COLOR)
    }
    
    @IBAction private func sliderTouchEnded(sender: UISlider) {
        
        self.setSlider(sender, highlightColor: CONTROL_NORMAL_COLOR)
    }
    
    //MARK:- UI
    private func runStillImageCaptureAnimation() {
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.previewView.layer.opacity = 0
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.previewView.layer.opacity = 1
            })
        })
    }
    
    private func configureManualHUD() {
        // 수동 초점 컨트롤
        self.focusModeControl.selectedSegmentIndex = find(self.focusModes, self.videoDevice.focusMode.rawValue)!
        
        for mode in self.focusModes {
            self.focusModeControl.setEnabled(self.videoDevice.isFocusModeSupported(AVCaptureFocusMode(rawValue: mode.integerValue)!), forSegmentAtIndex: find(self.focusModes, mode.integerValue)!)
        }
        
        self.lensPositionSlider.minimumValue = 0.0
        self.lensPositionSlider.maximumValue = 1.0
        self.lensPositionSlider.enabled = self.videoDevice.focusMode == AVCaptureFocusMode.Locked
        
        // 수동 노출 컨트롤
        for mode in self.exposureModes {
            self.exposureModeControl.setEnabled(self.videoDevice.isExposureModeSupported(AVCaptureExposureMode(rawValue: mode.integerValue)!), forSegmentAtIndex: find(self.exposureModes, mode)!)
        }
        
        // 0~1 사이의 슬라이더 범위를 사용하고, 슬라이더의 값과 실제 기기 노출 시간과 비선형 맵핑을 합니다.
        self.exposureDurationSlider.minimumValue = 0.0
        self.exposureDurationSlider.maximumValue = 1.0
        self.exposureDurationSlider.enabled = (self.videoDevice.exposureMode == AVCaptureExposureMode.Custom)
        
        self.ISOSlider.minimumValue = self.videoDevice.activeFormat.minISO
        self.ISOSlider.maximumValue = self.videoDevice.activeFormat.maxISO
        self.ISOSlider.enabled = (self.videoDevice.exposureMode == AVCaptureExposureMode.Custom)
        
        self.exposureTargetBiasSlider.minimumValue = self.videoDevice.minExposureTargetBias
        self.exposureTargetBiasSlider.maximumValue = self.videoDevice.maxExposureTargetBias
        self.exposureTargetBiasSlider.enabled = true
        
        self.exposureTargetOffsetSlider.minimumValue = self.videoDevice.minExposureTargetBias
        self.exposureTargetOffsetSlider.maximumValue = self.videoDevice.maxExposureTargetBias
        self.exposureTargetOffsetSlider.enabled = true
        
        // 수동 화이트밸런스 컨트롤
        self.whiteBalanceModeControl.selectedSegmentIndex = find(self.whiteBalanceModes, self.videoDevice.whiteBalanceMode.rawValue)!
        
        for mode in self.whiteBalanceModes {
            self.whiteBalanceModeControl.setEnabled(self.videoDevice.isWhiteBalanceModeSupported(AVCaptureWhiteBalanceMode(rawValue: mode.integerValue)!), forSegmentAtIndex: find(self.whiteBalanceModes, mode)!)
        }
        
        self.temperatureSlider.minimumValue = 3000
        self.temperatureSlider.maximumValue = 8000
        self.temperatureSlider.enabled = (self.videoDevice.whiteBalanceMode == AVCaptureWhiteBalanceMode.Locked)
        
        self.tintSlider.minimumValue = -150
        self.tintSlider.maximumValue = 150
        self.tintSlider.enabled = (self.videoDevice.whiteBalanceMode == AVCaptureWhiteBalanceMode.Locked)
    }
    
    private func positionManualHUD() {
        // 수동 조작 화면은 한 번에 하나만 보여주기 때문에 우리는 이 화면들을 모두 맨 위에 위치시킵니다.
        self.manualHUDExposureView.frame = CGRectMake(self.manualHUDFocusView.frame.origin.x, self.manualHUDFocusView.frame.origin.y, self.manualHUDExposureView.frame.size.width, self.manualHUDExposureView.frame.size.height)
        
        self.manualHUDWhiteBalanceView.frame = CGRectMake(self.manualHUDFocusView.frame.origin.x, self.manualHUDFocusView.frame.origin.y, self.manualHUDWhiteBalanceView.frame.size.width, self.manualHUDWhiteBalanceView.frame.size.height)
    }
    
    private func setSlider(slider: UISlider, highlightColor color:UIColor) {
        slider.tintColor = color
        
        switch slider {
        case self.lensPositionSlider:
            self.lensPositionNameLabel.textColor = slider.tintColor
            self.lensPositionValueLabel.textColor = slider.tintColor
            
        case self.exposureDurationSlider:
            self.exposureDurationNameLabel.textColor = slider.tintColor
            self.exposureDurationValueLabel.textColor = slider.tintColor

        case self.ISOSlider:
            self.ISONameLabel.textColor = slider.tintColor
            self.ISOValueLabel.textColor = slider.tintColor

        case self.exposureTargetBiasSlider:
            self.exposureTargetBiasNameLabel.textColor = slider.tintColor
            self.exposureTargetBiasValueLabel.textColor = slider.tintColor

        case self.temperatureSlider:
            self.temperatureNameLabel.textColor = slider.tintColor
            self.temperatureValueLabel.textColor = slider.tintColor

        case self.tintSlider:
            self.tintNameLabel.textColor = slider.tintColor
            self.tintValueLabel.textColor = slider.tintColor
            
        default:
            break
        }
    }
    
    //MARK:- File Output Delegate
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!) {
        
        if let err = error {
            println(err)
        }
        
        self.lockInterfaceRotation = false
        
        // backgroundRecordingID는 ALAssetsLibrary completion handler 에서 백그라운드 테스크를 종료하기 위해 사용함을 기억해 두십시오.
        let backgroundRecordingID: UIBackgroundTaskIdentifier = self.backgroundRecordingID
        self.backgroundRecordingID = UIBackgroundTaskInvalid
        
        ALAssetsLibrary().writeVideoAtPathToSavedPhotosAlbum(outputFileURL, completionBlock: { (assetURL:NSURL!, error: NSError!) -> Void in
            if let err = error {
                println(err)
            }
            
            NSFileManager.defaultManager().removeItemAtURL(outputFileURL, error: nil)
            
            if backgroundRecordingID != UIBackgroundTaskInvalid {
                UIApplication.sharedApplication().endBackgroundTask(backgroundRecordingID)
            }
        })
    }
    
    //MARK:- Device Configuration
    private func focusAndExposeWith(focusMode: AVCaptureFocusMode, exposureMode: AVCaptureExposureMode, atDevicePoint point:CGPoint, monitorSubjectAreaChange: Bool) {
        dispatch_async(self.sessionQueue, { () -> Void in
            let device = self.videoDevice
            var error: NSError? = nil
            
            if device.lockForConfiguration(&error) {
                if device.focusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
                    device.focusMode = focusMode
                    device.focusPointOfInterest = point
                }
                if device.exposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
                    device.exposureMode = exposureMode
                    device.exposurePointOfInterest = point
                }
                device.subjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
                device.unlockForConfiguration()
            } else if let err = error {
                println(err)
            }
        })
    }
    
    private class func setFlashMode(flashMode: AVCaptureFlashMode, forDevice device:AVCaptureDevice) {
        
        if device.hasFlash && device.isFlashModeSupported(flashMode) {
            var error: NSError? = nil
            
            if device.lockForConfiguration(&error) {
                device.flashMode = flashMode
                device.unlockForConfiguration()
            } else if let err = error {
                println(err)
            }
        }
    }
    
    private func setWhiteBalanceGains(gains: AVCaptureWhiteBalanceGains) {

        var error: NSError? = nil

        if self.videoDevice.lockForConfiguration(&error) {
            let normalizedGains = self.normalizedGains(gains)// 범위를 벗어난 값은 사용할 수 없으니 제대로 된 값을 넣어줍니다.
            self.videoDevice.setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains(normalizedGains, completionHandler: nil)
        } else if let err = error {
            println(err)
        }
    }
    
    
    //MARK:- KVO
    private func addObservers() {
        self.addObserver(self, forKeyPath: "sessionRunningAndDeviceAuthorized", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &SessionRunningAndDeviceAuthorizedContext)
        self.addObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &CapturingStillImageContext)
        self.addObserver(self, forKeyPath: "movieFileOutput.recording", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &RecordingContext)
        
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.focusMode", options: (NSKeyValueObservingOptions.Initial | NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &FocusModeContext)
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.lensPosition", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &LensPositionContext)
        
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.exposureMode", options: (NSKeyValueObservingOptions.Initial | NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &ExposureModeContext)
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.exposureDuration", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &ExposureDurationContext)
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.ISO", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &ISOContext)
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.exposureTargetOffset", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &ExposureTargetOffsetContext)
        
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.whiteBalanceMode", options: (NSKeyValueObservingOptions.Initial | NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &WhiteBalanceModeContext)
        self.addObserver(self, forKeyPath: "videoDeviceInput.device.deviceWhiteBalanceGains", options: (NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New), context: &DeviceWhiteBalanceGainsContext)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "subjectAreaDidChange:", name: AVCaptureDeviceSubjectAreaDidChangeNotification, object: self.videoDevice)
        
        weak var weakSelf = self
        
        self.runtimeErrorHandlingObserver = NSNotificationCenter.defaultCenter().addObserverForName(AVCaptureSessionRuntimeErrorNotification, object: self.session, queue: nil, usingBlock: { (noti: NSNotification!) -> Void in
            var strongSelf: ViewController = weakSelf! as ViewController
            
            dispatch_async(strongSelf.sessionQueue, { () -> Void in
                strongSelf.session.startRunning()
                strongSelf.recordButton.setTitle("Record", forState: UIControlState.Normal)
            })
        })
    }
    
    private func removeObservers() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVCaptureDeviceSubjectAreaDidChangeNotification, object: self.videoDevice)
        if let handler: AnyObject = self.runtimeErrorHandlingObserver {
            NSNotificationCenter.defaultCenter().removeObserver(handler)
        }
        
        self.removeObserver(self, forKeyPath: "sessionRunningAndDeviceAuthorized", context: &SessionRunningAndDeviceAuthorizedContext)
        self.removeObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", context: &CapturingStillImageContext)
        self.removeObserver(self, forKeyPath: "movieFileOutput.recording", context: &RecordingContext)
        
        self.removeObserver(self, forKeyPath: "videoDevice.focusMode", context: &FocusModeContext)
        self.removeObserver(self, forKeyPath: "videoDevice.lensPosition", context: &LensPositionContext)
        
        self.removeObserver(self, forKeyPath: "videoDevice.exposureMode", context: &ExposureModeContext)
        self.removeObserver(self, forKeyPath: "videoDevice.exposureDuration", context: &ExposureDurationContext)
        self.removeObserver(self, forKeyPath: "videoDevice.ISO", context: &ISOContext)
        self.removeObserver(self, forKeyPath: "videoDevice.exposureTargetOffset", context: &ExposureTargetOffsetContext)
        
        self.removeObserver(self, forKeyPath: "videoDevice.whiteBalanceMode", context: &WhiteBalanceModeContext)
        self.removeObserver(self, forKeyPath: "videoDevice.deviceWhiteBalanceGains", context: &DeviceWhiteBalanceGainsContext)
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        if context == &FocusModeContext {
            if let oldValue: Int = change[NSKeyValueChangeOldKey]?.integerValue {
                if let newValue: Int = change[NSKeyValueChangeNewKey]?.integerValue {
                    let oldMode: AVCaptureFocusMode = AVCaptureFocusMode(rawValue: oldValue)!
                    let newMode: AVCaptureFocusMode = AVCaptureFocusMode(rawValue: newValue)!
                    println("focus mode: \(self.stringFromFocusMode(oldMode)) -> \(self.stringFromFocusMode(newMode))")
                    
                    self.focusModeControl.selectedSegmentIndex = find(self.focusModes, newMode.rawValue)!
                    self.lensPositionSlider.enabled = (newMode == AVCaptureFocusMode.Locked)
                }
            }
        } else if context == &LensPositionContext {
            let newLensPosition: Float = change[NSKeyValueChangeNewKey]!.floatValue!
            
            if self.videoDevice.focusMode != AVCaptureFocusMode.Locked {
                self.lensPositionSlider.value = newLensPosition
            }
            self.lensPositionValueLabel.text = NSString(format: "%.1f", newLensPosition) as String
        } else if context == &ExposureModeContext {
            if let oldValue: Int = change[NSKeyValueChangeOldKey]?.integerValue {
                if let newValue: Int = change[NSKeyValueChangeNewKey]?.integerValue {
                    let oldMode: AVCaptureExposureMode = AVCaptureExposureMode(rawValue: oldValue)!
                    let newMode: AVCaptureExposureMode = AVCaptureExposureMode(rawValue: newValue)!
                    
                    println("exposure mode: \(self.stringFromExposureMode(oldMode)) -> \(self.stringFromExposureMode(newMode))")
                    
                    self.exposureModeControl.selectedSegmentIndex = find(self.exposureModes, newMode.rawValue)!
                    self.exposureDurationSlider.enabled = (newMode == AVCaptureExposureMode.Custom)
                }
            }
        } else if context == &ExposureDurationContext {
            let newDurationSeconds: Double = CMTimeGetSeconds(change[NSKeyValueChangeNewKey]!.CMTimeValue)
            
            if self.videoDevice.exposureMode != AVCaptureExposureMode.Custom {
                let minDurationSeconds: Double = max(CMTimeGetSeconds(self.videoDevice.activeFormat.minExposureDuration), EXPOSURE_MINIMUM_DURATION)
                
                let maxDurationSeconds: Double = CMTimeGetSeconds(self.videoDevice.activeFormat.maxExposureDuration)
                
                let p: Double = (newDurationSeconds - minDurationSeconds) / (maxDurationSeconds - minDurationSeconds)
                
                self.exposureDurationSlider.value = Float(pow(p, 1 / EXPOSURE_DURATION_POWER))
                
                if newDurationSeconds < 1 {
                    let digits: Int = max(0, Int(floor(log10(newDurationSeconds))))
                    self.exposureDurationValueLabel.text = NSString(format: "1/%.*f", digits, 1 / newDurationSeconds) as String
                } else {
                    self.exposureDurationValueLabel.text = NSString(format: "%.2f", newDurationSeconds) as String
                }
            }
        } else if context == &ISOContext {
            let newISO: Float = change[NSKeyValueChangeNewKey]!.floatValue!
            
            if self.videoDevice.exposureMode != AVCaptureExposureMode.Custom {
                self.ISOSlider.value = newISO
            }
            self.ISOValueLabel.text = NSString(format: "%i", Int(newISO)) as String
        } else if context == &ExposureTargetOffsetContext {
            let newExposureTargetOffset = change[NSKeyValueChangeNewKey]!.floatValue!
            
            self.exposureTargetOffsetSlider.value = newExposureTargetOffset
            self.exposureTargetOffsetValueLabel.text = NSString(format: "%.1f", newExposureTargetOffset) as String
        } else if context == &WhiteBalanceModeContext {
            if let oldValue: Int = change[NSKeyValueChangeOldKey]?.integerValue {
                if let newValue: Int = change[NSKeyValueChangeNewKey]?.integerValue {
                    let oldMode: AVCaptureWhiteBalanceMode = AVCaptureWhiteBalanceMode(rawValue: oldValue)!
                    let newMode: AVCaptureWhiteBalanceMode = AVCaptureWhiteBalanceMode(rawValue: newValue)!
                    
                    println("white balance mode: \(self.stringFromWhiteBalanceMode(oldMode)) -> \(self.stringFromWhiteBalanceMode(newMode))")
                    
                    self.whiteBalanceModeControl.selectedSegmentIndex = find(self.whiteBalanceModes, newMode.rawValue)!
                    self.temperatureSlider.enabled = (newMode == AVCaptureWhiteBalanceMode.Locked)
                    self.tintSlider.enabled = (newMode == AVCaptureWhiteBalanceMode.Locked)
                }
            }
        } else if context == &DeviceWhiteBalanceGainsContext {
            var newGains: AVCaptureWhiteBalanceGains = AVCaptureWhiteBalanceGainsCurrent
            change[NSKeyValueChangeNewKey]?.getValue(&newGains)
            
            let newTemperatureAndTint: AVCaptureWhiteBalanceTemperatureAndTintValues = self.videoDevice.temperatureAndTintValuesForDeviceWhiteBalanceGains(newGains)
            
            if self.videoDevice.whiteBalanceMode != AVCaptureWhiteBalanceMode.Locked {
                self.temperatureSlider.value = newTemperatureAndTint.temperature
                self.tintSlider.value = newTemperatureAndTint.tint
            }
            
            self.temperatureValueLabel.text = NSString(format: "%i", Int(newTemperatureAndTint.temperature)) as String
            self.tintValueLabel.text = NSString(format: "%i", Int(newTemperatureAndTint.tint)) as String
        }else if context == &CapturingStillImageContext {
            let isCapturingStillImage: Bool = change[NSKeyValueChangeNewKey]!.boolValue!
            
            if isCapturingStillImage {
                self.runStillImageCaptureAnimation()
            }
        } else if context == &RecordingContext {
            let isRecording: Bool = change[NSKeyValueChangeNewKey]!.boolValue!
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if isRecording {
                    self.cameraButton.enabled = false
                    self.recordButton.setTitle("Stop", forState: UIControlState.Normal)
                    self.recordButton.enabled = true
                } else {
                    self.cameraButton.enabled = true
                    self.recordButton.setTitle("Record", forState: UIControlState.Normal)
                    self.recordButton.enabled = true
                }
            })
        } else if context == &SessionRunningAndDeviceAuthorizedContext {
            let isRunning: Bool = change[NSKeyValueChangeNewKey]!.boolValue!
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if isRunning {
                    self.cameraButton.enabled = true
                    self.recordButton.enabled = true
                    self.stillButton.enabled = true
                } else {
                    self.cameraButton.enabled = false
                    self.recordButton.enabled = false
                    self.stillButton.enabled = false
                }
            })
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    // selector로 런타임에 지정해주는 함수는 @objc 키워드를 필요로 합니다.
    @objc private func subjectAreaDidChange(notification: NSNotification) {
        let devicePoint = CGPointMake(0.5, 0.5)
        self.focusAndExposeWith(AVCaptureFocusMode.ContinuousAutoFocus, exposureMode: AVCaptureExposureMode.ContinuousAutoExposure, atDevicePoint: devicePoint, monitorSubjectAreaChange: false)
    }

    //MARK:- Utilities
    private class func device(mediaType: String, preferringPosition position:AVCaptureDevicePosition) -> AVCaptureDevice {
       
        let devices = AVCaptureDevice.devicesWithMediaType(mediaType)
        var captureDevice: AVCaptureDevice = devices.first as! AVCaptureDevice
        
        for device in devices {
            if device.position == position {
                captureDevice = device as! AVCaptureDevice
                break
            }
        }
        return captureDevice
    }

    private func checkDeviceAuthorizationStatus() {
        let mediaType = AVMediaTypeVideo
        
        AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
            if granted {
                self.deviceAuthorized = true
            } else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let alert: UIAlertController = UIAlertController(title: "Alert", message: "App dosen't have permission to use the Camera", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
                    
                    alert.addAction(cancelAction)
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    self.deviceAuthorized = false
                })
            }
        })
    }
    
    private func stringFromFocusMode(focusMode: AVCaptureFocusMode) -> String {
        var string = "INVALID FOCUS MODE"
        
        switch focusMode {
        case AVCaptureFocusMode.Locked:
            string = "Locked"
            
        case AVCaptureFocusMode.AutoFocus:
            string = "Auto"

        case AVCaptureFocusMode.ContinuousAutoFocus:
            string = "ContinuousAuto"

        }
        
        return string
    }
    
    private func stringFromExposureMode(exposureMode: AVCaptureExposureMode) -> String {
        var string = "INVALID EXPOSURE MODE"
        
        switch exposureMode {
        case AVCaptureExposureMode.Locked:
            string = "Locked"

        case AVCaptureExposureMode.AutoExpose:
            string = "Auto"

        case AVCaptureExposureMode.ContinuousAutoExposure:
            string = "ContinuousAuto"

        case AVCaptureExposureMode.Custom:
            string = "Custom"

        }
        
        return string
    }
    
    
    private func stringFromWhiteBalanceMode(whiteBalanceMode: AVCaptureWhiteBalanceMode) -> String {
        var string = "INVALID WHITE BALANCE MODE"
        
        switch whiteBalanceMode {
        case AVCaptureWhiteBalanceMode.Locked:
            string = "Locked"

        case AVCaptureWhiteBalanceMode.AutoWhiteBalance:
            string = "Auto"

        case AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance:
            string = "ContinuousAuto"

        }
        
        return string
    }
    
    private func normalizedGains(gains: AVCaptureWhiteBalanceGains) -> AVCaptureWhiteBalanceGains {
        var g: AVCaptureWhiteBalanceGains = gains
        
        g.redGain = max(1.0, g.redGain);
        g.greenGain = max(1.0, g.greenGain);
        g.blueGain = max(1.0, g.blueGain);
        
        g.redGain = max(self.videoDevice.maxWhiteBalanceGain, g.redGain);
        g.greenGain = max(self.videoDevice.maxWhiteBalanceGain, g.greenGain);
        g.blueGain = max(self.videoDevice.maxWhiteBalanceGain, g.blueGain);
        
        return g
    }

    //MARK:- Gesture Recognizer Delegate
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {

        // 버튼의 액션을 제스쳐 인식이 먼저 채가지 않도록 버튼 영역을 터치했을 때 버튼에 이벤트를 보내줍니다.
        let location = touch.locationInView(gestureRecognizer.view)
        var returnValue = true

        if CGRectContainsPoint(self.stillButton.frame, location) {
            self.stillButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
            returnValue = false
        } else if CGRectContainsPoint(self.recordButton.frame, location) {
            self.recordButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
            returnValue = false
        } else if CGRectContainsPoint(self.cameraButton.frame, location) {
            self.cameraButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
            returnValue = false
        }

        return returnValue
    }

}

