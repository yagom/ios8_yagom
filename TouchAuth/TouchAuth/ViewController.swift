//
//  ViewController.swift
//  TouchAuth
//
//  Created by yagom on 2014. 12. 2..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {

    @IBAction func clickAuthButton(sender: UIButton) {
        
        // Local Auth 관리 context
        let context:LAContext = LAContext()
        
        // 에러를 담을 NSError 객체 포인터
        var error:NSError? = nil
        
        // 에러 메세지
        var errorMessage:String? = nil
        
        // 생체 정보를 통한 인증을 할 수 있는지 판별하기 위해 canEvaluatePolicy 함수를 사용하여 확인한다
        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error) {

            // 만약 에러가 발생하였다면
            if let actualError = error {

                // 에러메세지를 담아둔다
                errorMessage = actualError.localizedDescription
            }
            else    // 에러가 없다면
            {
                // 생체 정보를 통한 인증을 시도한다.
                context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: "Would you login using Touch ID?", reply: { (success:Bool, error:NSError!) -> Void in
                    
                    // 인증에 성공했다면
                    if success == true {
                        
                        // 성공 얼럿을 보여준다.
                        self.showAlert("Hellooooo!!!", message: "Login success")
                    }
                    else if let actualError = error {   // 에러가 발생했다면
                        
                        // 얼럿을 통해 에러메세지를 보여준다.
                        self.showAlert("Oooh...", message: "Error code " + String(actualError.code) + " : " + actualError.localizedDescription)
                        
                        // tip : 에러상황에 따른 적절한 처리를 하고싶다면 actualError.code의 코드값과 LAError로 정의된 enum 값을 비교해 보면 된다.
                    }
                })
            }
        } else {    // 생체 정보를 통한 인증을 할 수 없는 경우

            // 에러 정보가 있다면
            if let actualError = error {
                
                // 에러 메세지를 넣어준다
                errorMessage = actualError.localizedDescription
            } else {
                errorMessage = "Unknwon error"
            }
        }

        // 만약 에러메세지가 존재한다면
        if errorMessage != nil {
            
            // 얼럿을 통해 보여준다
            self.showAlert("ERROR", message: errorMessage!)
        }
    }
    
    // 얼럿을 보여주기 위한 함수
    func showAlert(title : String, message : String) {
        
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction:UIAlertAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler: { (okAction) -> Void in
            alert.dismissViewControllerAnimated(true, completion: nil)
        })
        
        alert.addAction(okAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

